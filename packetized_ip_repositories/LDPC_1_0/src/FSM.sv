`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12/11/2023 01:00:49 PM
// Design Name: 
// Module Name: FSM
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 4 states. FSM in system verilog with 4 synchronous stages. These will be:
//IDLE is the first state. All of the rest stages lead to IDLE if (input signal) ARESET will be asserted and the reset output signal is asserted.
//IN stage is the 2nd state. It is enabled if input signal out_done = 1. Then output signal trigger_in = 1.
//SYST is the 3rd state. It starts when input signal in_done = 1 and output signal ?. 
//OUT is 4th state. It starts when input signal code_output = 1, and then output signal trigger_out = 1.
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
module FSM(
  input logic ACLOCK,
  input logic ARESET,          // Reset signal
  input logic out_done,        // Input signal for IN state
  input logic in_done,         // Input signal for SYST state
  input logic code_syst,        // counter_syst = 4096
  input logic code_output,     // counter_syst = 5120
  output logic enable,
  output logic enable_accum,
  output logic trigger_in,     // Output signal for IN state
  output logic trigger_out,    // Output signal for OUT state
  //for VERIFICATION 
  output logic [2:0]current,    //allaksa apo  [1:0] se  [2:0]
  output logic [2:0]next
);

    // five states
    typedef enum logic [2:0] { //gray encoding 
        IDLE = 3'b011, 
        IN = 3'b100, 
        PREP = 3'b101, 
        SYST = 3'b000, 
        PAR = 3'b001, 
        OUT = 3'b010
    } state;
    
    state current_state, next_state;  
    //(* fsm_encoding = "none" *)state current_state, next_state;  
    assign current = current_state; //to undo verification
    assign next = next_state;       //to undo verification
    
    // 1st always for reset state
    always_ff @(posedge ACLOCK or negedge ARESET) begin: STATE_MEMORY
        if (!ARESET)
            current_state <= IDLE; // Initial state is IDLE
        else
            current_state <= next_state; // Move to next state
    end
    
    //2nd always for next state
    always_comb begin: NEXT_STATE_LOGIC
        unique case(current_state)
            IDLE : next_state = IN; //(out_done) ? IN : IDLE;
            IN : next_state = (in_done && out_done) ? PREP : IN; //allaksa
            PREP : next_state = SYST;                           //apla xreiazetai ena cycle nwritera na proetoimazei thn eisodo 
            SYST : next_state = (code_syst) ? PAR : SYST;
            PAR : next_state = (code_output) ? OUT : PAR;
            OUT : next_state = IDLE;
            default: next_state = IDLE;
        endcase
    
   end         
            
  // 3rd final always for 
  always_comb begin: OUTPUT_LOGIC
    unique case (current_state)
      IDLE: begin
        enable = 0;
        enable_accum =0;
        trigger_in = 0;
        trigger_out = 0;
      end
      IN: begin
        enable = 0;
        enable_accum =0;
        trigger_in = 0;
        trigger_out = 0;
      end
      PREP: begin 
        enable = 0;
        enable_accum =0;
        trigger_in = 1;
        trigger_out = 0;
      end
      SYST: begin
        enable = 1;
        enable_accum =1;
        trigger_in = 1;
        trigger_out = 1;
      end
      PAR: begin
        enable = 1;
        enable_accum =0;
        trigger_in = 0;
        trigger_out = 1;
      end
      OUT: begin
        enable = 0;
        enable_accum =0;
        trigger_in = 0;
        trigger_out = 0;
      end
      default: begin        //added for safety, no need. Same as IDLE
        enable = 0;
        enable_accum =0;
        trigger_in = 0;
        trigger_out = 0;
      end
    endcase
  end
endmodule

//me to par state einai ayto parakatw 
/*
`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12/11/2023 01:00:49 PM
// Design Name: 
// Module Name: FSM
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
module FSM(
  input logic ACLOCK,
  input logic ARESET,          // Reset signal
  input logic out_done,        // Input signal for IN state
  input logic in_done,         // Input signal for SYST state
  inout logic par_output,
  input logic code_output,     // counter_syst = 5120
  output logic trigger_in,     // Output signal for IN state
  output logic ch_output,      // Output signal for SYST and PAR states
  output logic trigger_out,    // Output signal for OUT state
  
  //for VERIFICATION 
  output logic counter_syst,      // counter_syst = 4096
  output logic counter_codeword,   //counter_syst = 5120
  output logic [2:0]current,
  output logic [2:0]next
);

    // five states
    typedef enum logic [2:0] {
        IDLE, IN, SYST, PAR, OUT
    } state;
    state current_state, next_state;  
    assign current = current_state; //to undo verification
    assign next = next_state;       //to undo verification
    
    // 1st always for reset state
    always_ff @(posedge ACLOCK or negedge ARESET) begin: STATE_MEMORY
        if (!ARESET)
            current_state <= IDLE; // Initial state is IDLE
        else
            current_state <= next_state; // Move to next state
    end
    
    //2nd always for next state
    always_comb begin: NEXT_STATE_LOGIC
        unique case(current_state)
            IDLE : next_state = (out_done) ? IN : IDLE;
            IN : next_state = (in_done) ? SYST : IN;
            SYST : next_state = (par_output) ? PAR : SYST;
            PAR : next_state = (code_output) ? OUT : PAR;
            OUT : next_state = IDLE;
            default: next_state = IDLE;
        endcase
    
   end         
            
  // 3rd final always for 
  always_comb begin: OUTPUT_LOGIC
    unique case (current_state)
      IDLE: begin
        trigger_in = 0;
        ch_output = 0;
        trigger_out = 0;
      end
      IN: begin
        trigger_in = 1;
        ch_output = 0;
        trigger_out = 0;
      end
      SYST: begin
        trigger_in = 0;
        ch_output = 0;
        trigger_out = 0;
      end
      PAR: begin
        trigger_in = 0;
        ch_output = 1;
        trigger_out = 0;
      end
      OUT: begin
        trigger_in = 0;
        ch_output = 0;
        trigger_out = 1;
      end
    endcase
  end
endmodule

*/
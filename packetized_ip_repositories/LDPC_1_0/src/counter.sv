`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/14/2023 12:51:43 AM
// Design Name: 
// Module Name: counter
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
module counter #(
  parameter TIMES = 128 )( //4   128)(      //when it will ring !!
  input logic clock,
  input logic reset,
  input logic enable,
  output logic ch_mode
);

    // Internal counter
    logic [$clog2(TIMES)-1:0] count;        //LOG2(TIMES)

    // Sequential logic for counting
    always_ff @(posedge clock or negedge reset) begin
        if (!reset)begin
            count = 0;
        end 
        else begin  
            if(enable) begin 
                if (count < (TIMES-1)) begin
                    ch_mode = 0;
                    count <= count + 1;
                end else begin
                    ch_mode <= 1;
                    count <= 4'b0; // Reset the counter when it reaches TIMES
                end
            end
            else begin
                ch_mode = 0;
                count <= 0; // Reset the counter when it reaches TIMES_2
            end
        end
    end
endmodule

/* KANONIKA AYTO KATW EXW ALLA DOKIMAZW KATI ALLO LOGW ENOW WARNNG STO SYNTHESIS , EPISHS AN DEN LEITOYRGHSEI TO COVERAGE NA TO EPANAFERW
    // Internal counter
    logic [$clog2(TIMES)-1:0] count;        //LOG2(TIMES)

    // Sequential logic for counting
    always_ff @(posedge clock or negedge reset) begin
        if (!reset)begin
            count = 0;
        end 
        else if(enable) begin 
            if (count < (TIMES-1)) begin
                ch_mode = 0;
                count <= count + 1;
            end else begin
                ch_mode <= 1;
                count <= 4'b0; // Reset the counter when it reaches TIMES
            end
        end 
        else begin
            ch_mode = 0;
            count <= 0; // Reset the counter when it reaches TIMES_2
        end
    end
endmodule
*/





/*
module counter_different #(
  parameter TIMES = 12      //clock when systematic output stops
)(
  input logic clock,
  output logic ch_mode,

    //gia VERIFICATION
  // Internal counter
  output logic [$clog2(TIMES):0] count        //LOG2(TIMES)
);

  always_comb begin
    //only when counter is 0, then ch_mode have to be activated and take next row
    ch_mode = (count == 0); 
  end

  // Sequential logic for counting
  always_ff @(posedge clock) begin
    if (count < (TIMES-1)) begin
      count <= count + 1;
    end else begin
      count <= 4'b0; // Reset the counter when it reaches TIMES
    end
  end

endmodule
*/
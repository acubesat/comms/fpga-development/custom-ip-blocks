`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/15/2023 12:37:59 AM
// Design Name: 
// Module Name: ldpc_top
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: DATAPATH
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments: ok ala den thelei akribvw counter. Not a problemright now
// 
//////////////////////////////////////////////////////////////////////////////////

module ldpc_top#(    
    parameter QC_COLLUMNS = 128,  //3, //,            //#collumns of each QC parity mini arrays
    parameter QC_ROWS = 128,    //4,     //          //#rows of mini QC parity arrays 
    parameter NUMBER_RCE = 128, //2,
    parameter N = 4096,   //8, //
    parameter K = 5120,   //10, //
    
    //gia qc_selector 
    parameter ACCUM_END = 8 //1 //otan teleivsei o end, mallon tha einai to 8
    )(
    input logic ACLOCK,
    input logic reset,
    input logic enable,
    input logic enable_accum,       //only for accum 
    input logic serial_input,
    output logic serial_output,
    output logic counter_syst,                      //gia counter = 4096
    output logic counter_codeword,                //gia counter = 5120
        
    //for rom
    output logic rom_enable,
    output logic [5-1:0] rom_addr,
    input logic [ 1024-1: 0]rom_mem_data
     );   

    localparam ROW_INPUTS = $clog2(QC_ROWS);
    localparam WIDTH = K-N;
    //localparam WIDTH = 8;
    localparam DEPTH_RAM = N / QC_ROWS;
    localparam WIDTH_SEL = $clog2(DEPTH_RAM);      // log 2 of 32
    
    logic intra_syst_count_demux;   //wire that connects systematic counter to demuxs
    logic intra_prevRce_nextRce[NUMBER_RCE];
  
    logic [ WIDTH-1: 0]intra_ram_dff;
    
    logic [WIDTH_SEL-1:0]intra_accum_ram;
    
      //try for better performance
    logic intra_countacc_ram;
    
    //o counter me ton accumulator poy metrane kathe 128 (QC_ROWS) clocks kai aurhoizoyn tis fores poy exei xtyphsei 
    accum #(QC_ROWS, WIDTH_SEL, ACCUM_END)accum_dut(
      .clock(ACLOCK),
      .reset(reset),
      .enable(enable_accum),   //(!intra_syst_count_demux),
      .accum_out(intra_accum_ram)
      
      //try for better performance      //den leitoyrghse isws to bgalw 
//      .count_out(intra_countacc_ram)
    );
    
    
    //lets try sth like a gated clock 
    //logic gated_clock;
    //assign gated_clock  = ACLOCK && enable_accum;
    
    //inputs and wiring by integrated memory instead of bram by me
    assign rom_enable = enable_accum;
    assign rom_addr = intra_accum_ram;
    assign intra_ram_dff = rom_mem_data;
    
    /*
    //Bram that holds values of each row of each QC array
    ram_init_file #(WIDTH, DEPTH_RAM) ram_init_file_d (
        .clk(ACLOCK),             //to allazw apo ACLOCK se gated_clock mhpws kai allaksei tis times se power ontws peftei 
        .rd_en(enable_accum),     //to allazw apo enable_accum se intra_countacc_ram
        .addr(intra_accum_ram), 
        .dout(intra_ram_dff)
    );
    */
    logic[12:0] count;
    
    assign counter_syst = intra_syst_count_demux;
    //one counter to count systematic length and codeword length
    counter_modified #(N+1, K+1) counter_modified_dut (        //prepei na einai to prwo +1 giati xanw 1 kyklo sthn arxh apo ta 2 ff poy yparxoyn. 
        .clock(ACLOCK),
        .reset(reset),
        .enable(enable),
        .out_one(intra_syst_count_demux),
        .out_two(counter_codeword),
        .count(count)
    );
    
    // First rce
    rce #(QC_COLLUMNS) rce_dut(
        .row_values(intra_ram_dff[ (QC_COLLUMNS-1):0 ]),   //intra_ram_dff[WIDTH-1:(WIDTH-1 - QC_COLLUMNS)+1 ]),    //attention : From right to left all the values of each QC array
        .enable(enable),
        .enable_dff(!intra_syst_count_demux),
        .rce_in(0),
        .rce_out(intra_prevRce_nextRce[0]),
        .clock(ACLOCK ),
        .reset(reset),
        .msg_in(serial_input),
        .system_par(intra_syst_count_demux)
    );
    
    genvar i;
    generate
    //all the rest of rces
    for (i = 1; i <= NUMBER_RCE-1 ; i++) begin :rce_ds
        rce #(QC_COLLUMNS) rce_dut(
            .row_values(intra_ram_dff[ ((i+1)* QC_COLLUMNS) -1 -: QC_COLLUMNS ]),//(WIDTH-1)-i*QC_COLLUMNS : ((WIDTH-1)-(i +1)*QC_COLLUMNS)+1 ]),      //attention : From right to left all the values of each QC array
            .enable(enable),
            .enable_dff(!intra_syst_count_demux),
            .rce_in(intra_prevRce_nextRce[i-1]),
            .rce_out(intra_prevRce_nextRce[i]),
            .clock(ACLOCK ),
            .reset(reset),
            .msg_in(serial_input),
            .system_par(intra_syst_count_demux)
        ); 
    end
    endgenerate
    
    
    //Final demux for output 
    /*
    demux #(1,1) demux_d(
        .a(serial_input),
        .b(intra_prevRce_nextRce[NUMBER_RCE-1]),
        .reset(reset),
        .choise(intra_syst_count_demux),
        .out(serial_output) //intra_demux_dff )       //serial_output)       // me to teliko DFF bazeis kanonika intra_demux_dff
    );
    */
    //better in synthesis
    always_comb begin 
        case (intra_syst_count_demux)
            1'b0:   serial_output = serial_input;
            1'b1:   serial_output = intra_prevRce_nextRce[NUMBER_RCE-1];
        endcase
    end
    
    /* 
    
    // kanonika prepei ena DFF sto telos gia hazards 
    wire intra_demux_dff;
    // Teliko D flip flop , prostateyei apo hazards / glitches 
    DFF #(1)dut(
        .D(intra_demux_dff),
        .clock(ACLOCK),
        .enable(enable),
        .Q(serial_output),
        .reset(reset)
    );
    */
endmodule

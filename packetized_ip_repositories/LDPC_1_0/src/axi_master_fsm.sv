`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/07/2024 04:30:35 PM
// Design Name: 
// Module Name: axi_master_fsm
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
module axis_master_fsm(
    input logic ACLOCK,
    input logic ARESETN,          // Reset signal negative high
    output logic axis_mVALID,
    input logic axis_mREADY,    
    output logic wr_en,
    output logic rd_en,             //enable gia read: meaning, as long as rd_en == 1 bram give sout data
    output logic rd_bram,
    output logic start_over,
    output logic ptr_enable,
    input logic sp_fifo_empty,
    input logic sp_fifo_full,
    input logic trigger_out,     //encoder starts giving out data //syst = 1
    output logic out_done,
    
    //ebala egw grigora sto ergashtrio
    output logic rd_next             //next gia read: meaning, as long as rd_next == 1 addr of bram ++
    );
    
    // 3 states
    typedef enum logic [2:0] {  //gray encoding for better power consumption
        IDLE = 4'b0000, 
        RECEIVE_MODE = 4'b0001, 
        PUSHING_OUT_PREPARATION_1 = 4'b0011,
        PUSHING_OUT_PREPARATION_2 = 4'b0010,
        PUSHING_OUT_NO_READY = 4'b0110,
        PUSHING_OUT_WITH_READY = 4'b0111 
    } state;

    //(* fsm_encoding = "none" *)state current_state, next_state;  
    state current_state, next_state;  
    assign current = current_state; //to undo verification
    assign next = next_state;       //to undo verification
    
    // 1st always for reset state
    always_ff @(posedge ACLOCK or negedge ARESETN) begin: STATE_MEMORY
        if (!ARESETN)
            current_state <= IDLE; // Initial state is IDLE
        else
            current_state <= next_state; // Move to next state
    end
    
    //2nd always for next state
    always_comb begin: NEXT_STATE_LOGIC
        unique case(current_state)
            IDLE : next_state = (trigger_out) ? RECEIVE_MODE : IDLE;
            RECEIVE_MODE : next_state = (sp_fifo_full) ? PUSHING_OUT_PREPARATION_1 : RECEIVE_MODE;
            PUSHING_OUT_PREPARATION_1 : next_state = PUSHING_OUT_PREPARATION_2;
            PUSHING_OUT_PREPARATION_2 : begin 
                if ( axis_mREADY ) begin  
                    next_state = PUSHING_OUT_WITH_READY;
                end
                else begin 
                    next_state = PUSHING_OUT_NO_READY;
                end
            end
            PUSHING_OUT_NO_READY : begin 
                if (sp_fifo_empty) begin 
                    next_state = IDLE;
                end
                else begin 
                    if ( axis_mREADY ) begin  
                        next_state = PUSHING_OUT_PREPARATION_2;
                    end
                    else begin 
                        next_state = PUSHING_OUT_NO_READY;
                    end            
                end
            end
            PUSHING_OUT_WITH_READY : begin 
                if (sp_fifo_empty) begin 
                    next_state = IDLE;
                end
                else begin 
                    if ( axis_mREADY ) begin  
                        next_state = PUSHING_OUT_WITH_READY;
                    end
                    else begin 
                        next_state = PUSHING_OUT_NO_READY;
                    end
                end
            end
            default: next_state = IDLE;
        endcase
   end         
            
  // 3rd final always for OUTPUT_LOGIC
  always_comb begin: OUTPUT_LOGIC
    unique case (current_state)
      IDLE: begin
          axis_mVALID = 0;
          ptr_enable = 0;
          wr_en = 0;
          rd_en = 0;
          rd_bram = 0;
          start_over = 1;
          //axis_mVALID = 0;
          out_done = 1;
          rd_next = 0;
      end
      RECEIVE_MODE: begin
          axis_mVALID = 0;      
          wr_en = 1;
          rd_en = 0;
          rd_bram = 0;            
          start_over = 0;
          ptr_enable = 0;
          //axis_mVALID = 0;
          out_done = 0;
          rd_next = 0;
      end
      PUSHING_OUT_PREPARATION_1: begin
          axis_mVALID = 0;
          wr_en = 0;
          start_over = 0;
          ptr_enable = 1;
          out_done = 0;
          rd_en = 1;
          rd_bram = 1;               
          rd_next = 0;
      end
      PUSHING_OUT_PREPARATION_2: begin
          axis_mVALID = 0;
          wr_en = 0;
          start_over = 0;
          ptr_enable = 1;
          out_done = 0;
          rd_en = 1;
          rd_bram = 1;               
          rd_next = 1;
      end
      PUSHING_OUT_NO_READY: begin
          axis_mVALID = 1;       
          wr_en = 0;
          start_over = 0;
          ptr_enable = 1;
          out_done = 0;
          rd_en = 0;
          rd_next = 0;
          rd_bram = 1;               
      end
      PUSHING_OUT_WITH_READY: begin
          axis_mVALID = 1;       
          wr_en = 0;
          start_over = 0;
          ptr_enable = 1;
          out_done = 0;
          rd_en = 1;
          rd_bram = 1;          
          rd_next = 1;
      end      
          /*
          if ( axis_mREADY ) begin  // normally its: "if (axis_mREADY && axis_mVALID) begin" but axis_mVALID is always 1. For verif expression coverage.
              rd_next = 1;
          end 
          else begin 
              rd_next = 0;
          end
          */
    endcase
  end
endmodule

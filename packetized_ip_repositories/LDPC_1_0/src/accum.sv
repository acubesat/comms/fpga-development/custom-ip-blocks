`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12/18/2023 11:35:16 AM
// Design Name: 
// Module Name: accum
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module accum #(
  parameter TIMES = 128,        //4,
  parameter WIDTH = 5,          //  4,
  parameter ACCUM_END = 128         //4   //128)(      //when it will ring !!
   )( 
  input logic clock,
  input logic reset,
  input logic enable,
  output logic [WIDTH-1:0]accum_out,
  
  //try for better performance
  output logic count_out
    );
    
    logic intra_count_accum;
    assign count_out = intra_count_accum;
    
    counter #(TIMES) counter_dut(
        .clock(clock),
        .enable(enable),
        .reset(reset),
        .ch_mode(intra_count_accum)
    );

    // Sequential logic for counting
    always_ff @(posedge intra_count_accum or negedge reset) begin
        if (!reset)begin
            accum_out = 0;
        end 
        else begin 
            /* this code was here for robustness. But it was extracted due to the aim of 100% branch coverage. 
            if (accum_out == (ACCUM_END)) begin
                accum_out = 0;
            end else begin
                accum_out <= accum_out + 1;
            end
            */ 
            accum_out <= accum_out + 1;
        end
    end
endmodule


`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/27/2023 02:11:50 AM
// Design Name: 
// Module Name: demux
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:     IT WORKS 
// 
//////////////////////////////////////////////////////////////////////////////////


module demux#(
    parameter SIZE_A = 1,   //1   4
    parameter SIZE_B = 1)(  //1   4
    input logic [SIZE_A-1:0]a,
    input logic [SIZE_B-1:0]b,
    input logic choise,
    input logic reset,
    output logic [SIZE_A-1:0]out);
    
    always_comb begin
        if(!reset)
            out <= 0;
        else
            out <= (choise)? b : a;
    end
endmodule

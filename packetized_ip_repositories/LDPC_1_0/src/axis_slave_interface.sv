`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/02/2024 04:41:01 PM
// Design Name: 
// Module Name: axis_slave_interface
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: FSM-IN for axi4 stream slave interface
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module axis_slave_fsm(
    input logic ACLOCK,
    input logic ARESETN,          // Reset signal negative high
    input logic axis_sVALID,
    output logic axis_sREADY,    
    output logic wr_en,
    output logic rd_en,
    output logic start_over,
    input logic ps_fifo_full,
    input logic ps_fifo_empty,      //new
    input logic trigger_in,         // SIGNAL WHICH SAYS THAT ENCODER CAN NOW TAKE INPUT VALUES
    output logic in_done
    );
    
    assign axis_sREADY = ~ps_fifo_full;
    assign in_done = ps_fifo_full; //prosoxh allaksa apo full se empty
    // 4 states         //try BLANK
    typedef enum logic [2:0] { //gray encoding 
        IDLE = 3'b000, 
        BLANK = 3'b001, 
        RECEIVE_MODE = 3'b011, 
        WAIT_TO_PUSH_IN = 3'b010, 
        PUSH_IN = 3'b110
    } state;

    state current_state, next_state;  
    //(* fsm_encoding = "none" *) state current_state, next_state;  
    assign current = current_state; //to undo verification
    assign next = next_state;       //to undo verification
    
    // 1st always for reset state
    always_ff @(posedge ACLOCK or negedge ARESETN) begin: STATE_MEMORY
        if (!ARESETN)
            current_state <= IDLE; // Initial state is IDLE
        else
            current_state <= next_state; // Move to next state
    end
    
    //2nd always for next state
    always_comb begin: NEXT_STATE_LOGIC
        unique case(current_state)
            IDLE : next_state = BLANK;// (ps_fifo_empty) ? BLANK : IDLE;
            //try 
            BLANK : next_state = RECEIVE_MODE;
            //try
            RECEIVE_MODE : next_state = (ps_fifo_full) ? WAIT_TO_PUSH_IN : RECEIVE_MODE;
            WAIT_TO_PUSH_IN : next_state = (trigger_in) ? PUSH_IN : WAIT_TO_PUSH_IN;
            PUSH_IN : next_state = (ps_fifo_empty) ? IDLE : PUSH_IN;
            default: next_state = IDLE;
        endcase
    
   end         
            
  // 3rd final always for 
  always_comb begin: OUTPUT_LOGIC
    unique case (current_state)
      IDLE: begin
        wr_en = 0;
        rd_en = 0;
        start_over = 1;
      end
      //try
      BLANK: begin
        wr_en = 0;
        rd_en = 0;
        start_over = 0;
      end
      //try
      RECEIVE_MODE: begin
        start_over = 0;
        if(axis_sVALID && axis_sREADY) begin
            wr_en = 1;
            rd_en = 0;
        end
        else begin
            wr_en = 0;
            rd_en = 0;
        end  
      end
      WAIT_TO_PUSH_IN: begin
        wr_en = 0;
        rd_en = 0;
        start_over = 0;
      end
      PUSH_IN: begin
        wr_en = 0;
        rd_en = 1;
        start_over = 0;
      end
    endcase
  end
endmodule


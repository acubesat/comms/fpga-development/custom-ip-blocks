`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03/26/2024 11:00:05 AM
// Design Name: 
// Module Name: fpga_top
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module fpga_top #(
    // Parameters for axi slaves
    parameter SERIAL_WIDTH = 1,
    parameter TDATA_WIDTH = 64,
    //for ldpc main encoder
    parameter QC_COLLUMNS = 128,//3; //128,            //#collumns of each QC parity mini arrays
    parameter QC_ROWS = 128, //4;     //128          //#rows of mini QC parity arrays 
    parameter NUMBER_RCE = 8, //2;   //8
    parameter N = 4096, //8;           //4096,
    parameter K = 5120, //10;           //5120
    parameter ACCUM_END = 128 //4; //otan teleivsei o end, mallon tha einai to 8
    )(
    input logic ACLOCK,
    input logic ARESETN,
    //slave interface
    input logic axis_sVALID,
    //master interface
    output logic axis_mVALID,
    input logic axis_mREADY,
    output logic [TDATA_WIDTH-1:0]axis_mTDATA
    );
   
    wire [TDATA_WIDTH-1:0]intra_axis_sTDATA;
    wire intra_axis_sREADY; 
    ldpc_2#( SERIAL_WIDTH, TDATA_WIDTH, QC_COLLUMNS, QC_ROWS, NUMBER_RCE, N, K, ACCUM_END ) ldpc_2_dut (
        .ACLOCK(ACLOCK),
        .ARESETN(ARESETN),
    //slave interface
        .axis_sVALID(axis_sVALID),
        .axis_sREADY(intra_axis_sREADY),
        .axis_sTDATA(intra_axis_sTDATA),
        //master interface
        .axis_mVALID(axis_mVALID),
        .axis_mREADY(axis_mREADY),
        .axis_mTDATA(axis_mTDATA)
    );
    
    preparator prep_d(
        .ACLOCK( ACLOCK ),
        .ARESETN( ARESETN ),
        //master interface
        .prep_axis_mVALID( axis_sVALID ),
        .prep_axis_mREADY( intra_axis_sREADY ),
        .prep_axis_mTDATA( intra_axis_sTDATA )
    );
endmodule

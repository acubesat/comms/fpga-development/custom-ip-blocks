`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12/18/2023 11:11:09 AM
// Design Name: 
// Module Name: counter_accum
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
module accum #(
  parameter TIMES = 4,
  parameter WIDTH = 4,
  parameter ACCUM_END = 4
   )( //128)(      //when it will ring !!
  input logic clock,
  input logic reset,
  input logic enable,
  output logic [WIDTH-1:0]accum_out
    );
    
    logic intra_count_accum;
    
    counter #(TIMES) counter_dut(
        .clock(clock),
        .enable(enable),
        .reset(reset),
        .ch_mode(intra_count_accum)
    );

    // Sequential logic for counting
    always_ff @(posedge intra_count_accum or negedge reset) begin
        if (!reset)begin
            accum_out = 0;
        end 
        else if(enable) begin 
            if (accum_out == (ACCUM_END)) begin
                accum_out = 0;
            end else begin
                accum_out <= accum_out + 1;
            end
        end 
        else begin
            accum_out <= 0; // Reset the counter when it reaches TIMES_2
        end
    end
endmodule

`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/05/2024 05:24:49 PM
// Design Name: 
// Module Name: sp_fifo_bram
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: Asymmetric port RAM. Read Wider than Write. Read Statement in loop
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
module sp_fifo_bram (clk, enaA, weA, enaB, addrA, addrB, diA, doB);
parameter WIDTHA = 1;               //input
parameter DEPTHA = 5120;      //12  5120;
//parameter ADDRWIDTHA = $clog2(DEPTHA);

parameter WIDTHB = 64;     //4   64          //output
parameter DEPTHB = 80;      //3      80;
//parameter ADDRWIDTHB = $clog2(DEPTHB);

localparam ADDRWIDTHA = $clog2(DEPTHA); 
localparam ADDRWIDTHB = $clog2(DEPTHB);

input clk;
input weA;
input enaA, enaB;
input [ADDRWIDTHA-1:0] addrA;
input [ADDRWIDTHB-1:0] addrB;
input [WIDTHA-1:0] diA;
output [WIDTHB-1:0] doB;
`define max(a,b) {(a) > (b) ? (a) : (b)}
`define min(a,b) {(a) < (b) ? (a) : (b)}

localparam maxSIZE = `max(DEPTHA, DEPTHB);
localparam maxWIDTH = `max(WIDTHA, WIDTHB);
localparam minWIDTH = `min(WIDTHA, WIDTHB);

localparam RATIO = maxWIDTH / minWIDTH;
localparam log2RATIO = $clog2(RATIO);

reg [minWIDTH-1:0] RAM [0:maxSIZE-1];
reg [WIDTHB-1:0] readB;

always @(posedge clk)
begin
//if (enaA) begin
//if (weA)
    //kanonika einai etsi alla egw tha to grapsw etsi 
if (enaA && weA) begin
RAM[addrA] <= diA;
end
end

//below code is fine. But with gated clock its not need to check whether rd_en is asserted since clk is triggered only when rd_en is asserted.
logic gated_clk;
logic enable_flop;

always_ff @(negedge clk) begin
  enable_flop <= enaB;
end
assign gated_clk = clk & enable_flop;

always @(posedge gated_clk) begin : ramread
//always_latch begin : ramread
    integer i;
    reg [log2RATIO-1:0] lsbaddr;
    if (enaB) begin
        for (i = 0; i < RATIO; i = i+1) begin
            lsbaddr = i;
            readB[(RATIO-i)*minWIDTH-1 -: minWIDTH] <= RAM[{addrB, lsbaddr}]; //allaksa from vivado official site to store values in reverse
        end
    end
    else begin 
        readB = 0;
    end
end
assign doB = readB;

endmodule


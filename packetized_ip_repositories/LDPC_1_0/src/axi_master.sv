`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/02/2024 06:05:17 PM
// Design Name: 
// Module Name: axi_slave
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module axi_master#(      
    // Parameters mainly for sp_fifo
    parameter WIDTHA =  1,              //input (write)
    parameter DEPTHA = 5120,   //12,   //5120;
    parameter WIDTHB = 64,     //4,//64          //output (read)
    parameter DEPTHB = 80      //3  80;
    )(
    //ports
    input logic ACLOCK,
    input logic ARESETN,            //active low panta, mhn ta ksanaleme
    output logic S_AXIS_TVALID,
    input logic S_AXIS_TREADY,
    output logic [WIDTHB-1:0]S_AXIS_TDATA,
    input logic trigger_out,       
    output logic out_done,
    input logic [WIDTHA-1:0] writen_data,    //data to be written
    
    output logic master_read_ptr 
 
    );
 
    wire intra_wr_en;
    wire intra_rd_en;
    wire intra_start_over;
    wire intra_fifo_full;
    wire intra_fifo_empty;
    wire intra_rd_next;
    wire intra_rd_bram;
    wire intra_ptr_enable;
            
    sp_fifo #(WIDTHA, DEPTHA, WIDTHB, DEPTHB) sp_fifo_d(
        .clock(ACLOCK),
        .reset(ARESETN),              //active low panta, mhn ta ksanaleme
        .wr_en(intra_wr_en),              //otan theloyn na grapsoyn
        .rd_en(intra_rd_en),
        .rd_bram(intra_rd_bram),
        .in_data(writen_data),          //to be stored/writen data
        .start_over(intra_start_over),
        .ptr_enable(intra_ptr_enable),
        .out_data(S_AXIS_TDATA),    //to be read data
        .sp_fifo_empty(intra_fifo_empty),
        .sp_fifo_full(intra_fifo_full),
        .axis_mREADY(S_AXIS_TREADY),
        //.axis_mVALID(S_AXIS_TVALID),
        .rd_next(intra_rd_next),
        
        .master_read_ptr(master_read_ptr)
    );
    
    axis_master_fsm axis_master_fsm_d (
        .ACLOCK(ACLOCK),
        .ARESETN(ARESETN),  
        .axis_mVALID(S_AXIS_TVALID),
        .axis_mREADY(S_AXIS_TREADY),
        .wr_en(intra_wr_en),
        .rd_en(intra_rd_en),
        .rd_bram(intra_rd_bram),
        .start_over(intra_start_over),
        .ptr_enable(intra_ptr_enable),
        .sp_fifo_empty(intra_fifo_empty),
        .sp_fifo_full(intra_fifo_full),
        .trigger_out(trigger_out),
        .out_done(out_done),
        .rd_next(intra_rd_next)
    );
endmodule

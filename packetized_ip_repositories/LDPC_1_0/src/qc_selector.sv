`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12/18/2023 11:31:13 PM
// Design Name: 
// Module Name: qc_selector
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module qc_selector#(
    parameter INPUTS = 8,  // Number of input lines (default: 8)
    parameter TIMES = 4,    //128
    parameter WIDTH = 4,    //
    parameter ACCUM_END = 4 //otan teleivsei o end, mallon tha einai to 8
    )(
    input logic [INPUTS-1:0] in,
    input logic clock,
    input logic reset,
    input logic enable,
    output logic out   
    );
    
    logic [WIDTH-1:0]intra_accum_demux;
    
    accum #(TIMES, WIDTH, ACCUM_END)accum_dut(
      .clock(clock),
      .reset(reset),
      .enable(enable),
      .accum_out(intra_accum_demux)
    );
     
    demux_modif #(INPUTS, WIDTH) demux_modif_dut (
        .in(in),
        .sel(intra_accum_demux),
        .enable(enable),
        .reset(reset),
        .out(out)
    );
    
    
endmodule

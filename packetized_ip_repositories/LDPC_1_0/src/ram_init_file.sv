`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/08/2024 01:20:25 PM
// Design Name: 
// Module Name: ram_init_file
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:     NA TO KSANATSEKARW ME OLES TIW TIMES GIATI ENW ARXIKA KATALABAINE TO BRAM OTAN ebgala to din kai write_enable kai meiwsa tis times den katalabainei tis bram
// 
//////////////////////////////////////////////////////////////////////////////////

// Initializing Block RAM from external data file
// hexademical data
// File: rams_init_file.v

module ram_init_file (clk, rd_en, addr, dout);
parameter WIDTH = 1024;
parameter DEPTH = 32;
localparam ADDR = $clog2(DEPTH);
input clk;
input rd_en;
input [ADDR-1:0] addr;
output [WIDTH-1:0] dout;

reg [WIDTH-1:0] ram [0:DEPTH-1];
reg [WIDTH-1:0] dout;

initial begin
    $readmemh("rams_init_file.txt",ram);
    //$readmemh("test.txt",ram);
end

//below code is fine. But with gated clock its not need to check whether rd_en is asserted since clk is triggered only when rd_en is asserted.
logic gated_clk;
logic enable_flop;

always_ff @(negedge clk) begin
  enable_flop <= rd_en;
end
assign gated_clk = clk & enable_flop;

always @(posedge gated_clk) // apo clk se gated_clk
    begin
        if (rd_en)
            dout <= ram[addr];
        else
            dout <= 0;
    end
 
/*
// So the above code is replaced with the below simpler block of code
always @(posedge clk) begin 
    dout <= ram[ addr ];
end
*/
endmodule

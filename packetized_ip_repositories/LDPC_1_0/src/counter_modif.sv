`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/14/2023 12:51:43 AM
// Design Name: 
// Module Name: counter
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: This module has 2 OUTPUT SIGNALS. First is triggered when TIMES_1 clock come and second when TIMES_2 ones come.
//              synchronous enable and asynchronous reset
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:     it works mexri stigmhs. SAN PARAMETROS MPAINEI O PALMOS POY THELW NA BGALEI SHMA
// 
//////////////////////////////////////////////////////////////////////////////////
module counter_modified #(
  parameter TIMES_1 = 4096,  //8,    //4096,
  parameter TIMES_2 = 5120   //10)(  //5120)(      //when it will ring !!
  )(  
  input logic clock,
  input logic reset,
  input logic enable,
  output logic out_one,
  output logic out_two,
  //GIA VERIFICATION
  output logic [$clog2(TIMES_2)-1:0] count //VERY USEFULL to track the cycle when encoding
);

    // Sequential logic for counting
    always_ff @(posedge clock or negedge reset) begin
        if (!reset)begin
            count = 0;
            out_one = 0;
            out_two = 0;
        end 
        else begin 
            if(enable) begin 
                if (count < (TIMES_2-1)) begin
                    out_two = 0;
                    out_one = (count >= (TIMES_1-1)) ? 1 : 0;    
                    count <= count + 1;
                end 
                else begin
                    out_two <= 1;
                    count <= 0; // Reset the counter when it reaches TIMES_2
                end
            end
            else begin 
                count = 0;
            end
        end
    end
endmodule

/*  KANONIKA AYTO KATW EXW ALLA DOKIMAZW KATI ALLO LOGW ENOW WARNNG STO SYNTHESIS , EPISHS AN DEN LEITOYRGHSEI TO COVERAGE NA TO EPANAFERW
    // Internal counter
    //logic [$clog2(TIMES_2):0] count;        //LOG2(TIMES)         ////to undo verification

    // Sequential logic for counting
    always_ff @(posedge clock or negedge reset) begin
        if (!reset)begin
            count = 0;
            out_one = 0;
            out_two = 0;
        end 
        else if(enable) begin 
            if (count < (TIMES_2-1)) begin
                out_two = 0;
                out_one = (count >= (TIMES_1-1)) ? 1 : 0;    
                count <= count + 1;
            end 
            else begin
                out_two <= 1;
                count <= 0; // Reset the counter when it reaches TIMES_2
            end
        end
        else 
            count = 0;
    end
endmodule
*/
`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03/25/2024 09:32:21 PM
// Design Name: 
// Module Name: preparator
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module preparator #(
    // Parameters for axi slaves
    parameter SERIAL_WIDTH = 1,
    parameter TDATA_WIDTH = 64,
    //for ldpc main encoder
    parameter QC_COLLUMNS = 128, //3,    //#collumns of each QC parity mini arrays
    parameter QC_ROWS = 128, //4,        //#rows of mini QC parity arrays 
    parameter NUMBER_RCE = 8, //2, 
    parameter N = 4096, //8, 
    parameter K = 5120 //10, 
    )(
    input logic ACLOCK,
    input logic ARESETN,
    //master interface
    input logic prep_axis_mVALID,
    input logic prep_axis_mREADY,
    output logic [TDATA_WIDTH-1:0]prep_axis_mTDATA
    );
    
    localparam SLAVE_DEPTH = N/TDATA_WIDTH;
    localparam MASTER_DEPTH = K/TDATA_WIDTH;

    always_ff@(posedge ACLOCK ) begin 
        if( !ARESETN ) begin 
            prep_axis_mTDATA <= 0;
        end 
        else if (prep_axis_mVALID && prep_axis_mREADY) begin 
            prep_axis_mTDATA <= 64'h12345678FA3B5C78;
        end
        else begin 
            prep_axis_mTDATA <= 0;
        end    
    end

endmodule

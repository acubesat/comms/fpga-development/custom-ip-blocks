`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/15/2023 12:39:53 AM
// Design Name: 
// Module Name: rce
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments: it works
// 
//////////////////////////////////////////////////////////////////////////////////

module rce#(
    parameter QC_COLLUMNS = 128,//4            //#collumns of each QC parity mini arrays
    //parameter QC_ROWS = 4               //#rows of mini QC parity arrays
    // DOKIMHHHHHHHH 
    parameter FIRST_RCE = 0 
    )(
    input logic [QC_COLLUMNS-1:0] row_values,  //QC_COLLUMNS values of first row of each QC array 
    input logic msg_in,      //1 bit serial
    input rce_in,            //same as rce_out
    input logic clock,
    input logic reset,
    input logic enable,
    input logic enable_dff,       //wire that enable specifficaly dffs above clock 
    input logic system_par,         // a flag to check whther its systematic or parity bits clock
    output logic rce_out    //1bit
    );
    
    logic intra_mul_xor[QC_COLLUMNS];     //wire between mul and xor 
    logic intra_reg_adder[QC_COLLUMNS];      //wire between reg and adder
    logic intra_xor_reg[QC_COLLUMNS];     //wire between xor and reg 
    logic intra_dff_and[QC_COLLUMNS];

    genvar i;
    
    //to prwto rce prepei na diathrei to row_value
    dlatch dlatch_d_first(
        .D(row_values[ QC_COLLUMNS -1 ]),//[ (QC_COLLUMNS -1) - i]),          //BE CAREFULL. ROW VALUES from right to left 
        .Q(intra_dff_and[0]),
        .reset(reset),
        .enable(enable_dff)
    );
    
    //dlatches before/above rce. Holds each bit of one row of QC arrays
    for (i = 1; i<= QC_COLLUMNS-1; i++) begin : dlatch_defore_ds
        dlatch dlatch_ds(
            .D(row_values[i-1]),//[ (QC_COLLUMNS -1) - i]),          //BE CAREFULL. ROW VALUES from right to left rotated kata 1 
            .Q(intra_dff_and[i]),
            .reset(reset),
            .enable(enable_dff)
        );
    end 
    
    for (i = 0; i<= QC_COLLUMNS-1; i++) begin :mul_ds //oloi oi mul QC_COLLUMNS sto plithos  
        multiplier mul_ds ( 
            .in_bit_1(msg_in),
            .in_bit_2(intra_dff_and[i]),                //a row value
            .reset(reset),
            .out_bit(intra_mul_xor[i])      
        );
    end       
    
    if (FIRST_RCE == 0) begin
        logic intra_demux_xor;
        //only the first adder
        xor_adder xor_adder_d (     
            .in_bit_1(intra_mul_xor[0]),
            .in_bit_2(intra_demux_xor),
            .reset(reset),
            .out_bit(intra_xor_reg[0])      
        );
        
        //only for the first adder there has an extra demux to check wheter there is recursive feedback or bits simply are shifted out
        /*
        demux#(1, 1) demux_dut(
            .a(intra_reg_adder[QC_COLLUMNS - 1]),  
            .b(rce_in),
            .choise(system_par),    //if system_par = 1 => b: shift
            .reset(reset),          //if system_par = 0 => a: recursive feedback
            .out(intra_demux_xor)
        );
        */
        //its better in synthesis. Check file synth.run> without _resets_demux
        always_comb begin 
            case (system_par)
                1'b0:   intra_demux_xor = intra_reg_adder[QC_COLLUMNS - 1];
                1'b1:   intra_demux_xor = rce_in;
            endcase
        end
    end
    else begin 
    
    end
    
    //ola ta adders ektos apo to prwto me (i=0))
    for (i = 1; i<= QC_COLLUMNS-1; i++) begin : xor_add_ds  
        xor_adder xor_adder_ds (
            .in_bit_1(intra_mul_xor[i]),
            .in_bit_2(intra_reg_adder[i-1]),    //i - 1 sigoyra giati o prwtos adder pairnei apo allo wire 
            .reset(reset),
            .out_bit(intra_xor_reg[i])      
        );
    end  
    
    //dffs inside rce
    for (i = 0; i<= QC_COLLUMNS-1; i++) begin : dff_inside_ds
        DFF #(1)dff_ds(
            .D(intra_xor_reg[i]),
            .clock(clock),
            .Q(intra_reg_adder[i]),
            .reset(reset),
            .enable(enable)
        );
    end
    
    //only one rce's output: one bit to the rext rce 
    assign rce_out = intra_reg_adder[QC_COLLUMNS-1];

endmodule

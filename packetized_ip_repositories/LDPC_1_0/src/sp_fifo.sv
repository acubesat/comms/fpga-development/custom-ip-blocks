`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12/29/2023 02:15:50 PM
// Design Name: 
// Module Name: sp_fifo
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
module sp_fifo #(
    // Parameters
    parameter WIDTHA = 1,              //input (write)
    parameter DEPTHA = 5120,      //12 5120;
    parameter WIDTHB = 64,     //4  64          //output (read)
    parameter DEPTHB = 80     //3   80;
    )( // 8 for verification // Depth of the module accum
    //inouts
    input logic clock,
    input logic reset,              //active low panta, mhn ta ksanaleme
    input logic wr_en,              //otan theloyn na grapsoyn
    input logic rd_en,             //allow change of pipeline "last ff chagges value"
    input logic rd_bram,             //enable read: meaning, as long as rd_en == 1 bram give sout data
    input logic start_over,
    input logic ptr_enable,
    input logic [WIDTHA-1:0] in_data,      //to be stored/writen data
    output logic [WIDTHB-1:0] out_data,    //to be read data
    output logic sp_fifo_empty,
    output logic sp_fifo_full,
    
    //ebala egw grigora sto ergashtrio
    input logic rd_next,             //next read: meaning, as long as rd_next == 1 addr of bram ++
    output logic axis_mVALID,
    input logic axis_mREADY, 
    
    //debig 
    output logic master_read_ptr 

);
    localparam ADDRWIDTHA = $clog2(DEPTHA);
    localparam ADDRWIDTHB = $clog2(DEPTHB);
    
    reg [ADDRWIDTHA-1:0] write_ptr;
    reg [ADDRWIDTHB-1:0] read_ptr;

    // Flags
    reg empty_flag;     //internal gia otan adeiasei
    reg full_flag;      //internal gia otan gemisei
    
    logic [WIDTHB-1:0] intra_data_bram_dff;
    logic [WIDTHB-1:0] inside_data;
    
    assign master_read_ptr = read_ptr;
    
    // Our bram 
    sp_fifo_bram #(WIDTHA, DEPTHA, WIDTHB, DEPTHB) sp_fifo_bram_d(
        .clk(clock), 
        .enaA(wr_en), 
        .weA(wr_en), 
        .enaB(rd_bram), 
        .addrA(write_ptr), 
        .addrB(read_ptr), 
        .diA(in_data), 
        .doB(intra_data_bram_dff)
    );
    
    //pipeline 
    always_ff @(posedge clock or negedge reset) begin 
        if (!reset) begin
             inside_data <= 0;
        end
        else begin 
            if ( rd_en || axis_mREADY ) begin 
                inside_data <= intra_data_bram_dff ; 
            end
            else begin 
                inside_data <= inside_data;
            end
        end     
    end
    assign out_data = inside_data;
    /* error
    //pipeline 
    always_ff @(posedge clock or negedge reset) begin 
        if ( rd_en || axis_mREADY ) begin 
            inside_data <= intra_data_bram_dff ; 
        end     
    end
    assign out_data = inside_data;
    */
    /*
    DFF #(WIDTHB) dff_after_bram (
        .reset ( reset),
        .clock( clock ),
        .enable (rd_en),
        .D(intra_data_bram_dff),
        .Q(out_data)
    );
    */
    // Write logic
    always_ff @(posedge clock or negedge reset) begin
        if (!reset) begin
            write_ptr <= 0;
        end else if (wr_en && !full_flag) begin
            write_ptr <= write_ptr + 1;
        end
        else if (start_over) begin 
            write_ptr <= 0;
        end 
    end

    // Read logic
        //always_ff @(posedge clock or negedge reset or posedge axis_mREADY) begin //to check if this works in synthesis added or posedge axis_mREADY
        always_ff @(posedge clock or negedge reset) begin
            if (!reset) begin
                read_ptr <= 0;
            end 
            else if (ptr_enable) begin  
                if ((rd_next || axis_mREADY) && !empty_flag) begin  
                    read_ptr <= read_ptr + 1;
                end
            end
            else begin 
                read_ptr <= 0;
            end
        end
 
        // Flags
    
    //try apo assign na kanw se ff . Now axi interface works perfect even in case of ending. And its better for glitches.
    //assign sp_fifo_empty = empty_flag;
    always_ff @(posedge clock) begin 
        sp_fifo_empty = empty_flag;
    end
    assign sp_fifo_full = full_flag;  
   
    
    // Empty and Full flags logic
    always_ff @(posedge clock) begin           //apo katw apo 2 se 1 
        empty_flag = (write_ptr == 0) || ((read_ptr == DEPTHB - 1) && (rd_next || axis_mREADY));  //allaksa apo rd_en se rd_en && axis_mREADY    //dialejh 7 sel 25 apo pshfiaka 2       //allaksa prin htan empty_flag = (write_ptr == 0) || (read_ptr == DEPTHB -1);  
        if ((write_ptr == (DEPTHA -1) -1 && wr_en)) begin
            full_flag =  1;
        end
        else if(write_ptr == (DEPTHA -1))
            full_flag = 1;
        else 
            full_flag = 0;
    end
    
    /*
    always_ff @(posedge clock) begin 
        axis_mVALID = rd_en;
    end
    */
endmodule
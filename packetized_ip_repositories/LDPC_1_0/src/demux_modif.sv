`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12/14/2023 08:33:37 PM
// Design Name: 
// Module Name: demux_modif
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
module demux_modif #(
  parameter INPUTS = 8,  // Number of input lines (default: 8)
  parameter SELECT = 4   // Number of select lines (default: 4)
)(
  input logic [0:INPUTS-1]in,
  input logic [SELECT-1:0] sel,
  input logic enable,
  input logic reset,
  output logic out
);

always_comb begin   
    if (!reset) begin
      out <= 0;
    end 
    else if (enable) begin
      out <= in[sel];        //out <= (sel < INPUTS) ? in[sel+:1] : 1'b0; 
      /*
    case (sel)
        4'b0000 : out <= in[0];
        4'b0001 : out <= in[1];
        4'b0010 : out <= in[2];
        4'b0011 : out <= in[3];
        4'b0100 : out <= in[4];
        4'b0101 : out <= in[5];
        4'b0110 : out <= in[6];
        4'b0111 : out <= in[7];
        default : out <= 0;
    endcase
      */
    end
    else 
        out <=0;
  end

endmodule


# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
  ipgui::add_param $IPINST -name "Component_Name"
  #Adding Page
  ipgui::add_page $IPINST -name "Page 0"


}

proc update_PARAM_VALUE.ACCUM_END { PARAM_VALUE.ACCUM_END } {
	# Procedure called to update ACCUM_END when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.ACCUM_END { PARAM_VALUE.ACCUM_END } {
	# Procedure called to validate ACCUM_END
	return true
}

proc update_PARAM_VALUE.K { PARAM_VALUE.K } {
	# Procedure called to update K when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.K { PARAM_VALUE.K } {
	# Procedure called to validate K
	return true
}

proc update_PARAM_VALUE.N { PARAM_VALUE.N } {
	# Procedure called to update N when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.N { PARAM_VALUE.N } {
	# Procedure called to validate N
	return true
}

proc update_PARAM_VALUE.NUMBER_RCE { PARAM_VALUE.NUMBER_RCE } {
	# Procedure called to update NUMBER_RCE when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.NUMBER_RCE { PARAM_VALUE.NUMBER_RCE } {
	# Procedure called to validate NUMBER_RCE
	return true
}

proc update_PARAM_VALUE.QC_COLLUMNS { PARAM_VALUE.QC_COLLUMNS } {
	# Procedure called to update QC_COLLUMNS when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.QC_COLLUMNS { PARAM_VALUE.QC_COLLUMNS } {
	# Procedure called to validate QC_COLLUMNS
	return true
}

proc update_PARAM_VALUE.QC_ROWS { PARAM_VALUE.QC_ROWS } {
	# Procedure called to update QC_ROWS when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.QC_ROWS { PARAM_VALUE.QC_ROWS } {
	# Procedure called to validate QC_ROWS
	return true
}

proc update_PARAM_VALUE.SERIAL_WIDTH { PARAM_VALUE.SERIAL_WIDTH } {
	# Procedure called to update SERIAL_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.SERIAL_WIDTH { PARAM_VALUE.SERIAL_WIDTH } {
	# Procedure called to validate SERIAL_WIDTH
	return true
}

proc update_PARAM_VALUE.TDATA_WIDTH { PARAM_VALUE.TDATA_WIDTH } {
	# Procedure called to update TDATA_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.TDATA_WIDTH { PARAM_VALUE.TDATA_WIDTH } {
	# Procedure called to validate TDATA_WIDTH
	return true
}


proc update_MODELPARAM_VALUE.SERIAL_WIDTH { MODELPARAM_VALUE.SERIAL_WIDTH PARAM_VALUE.SERIAL_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.SERIAL_WIDTH}] ${MODELPARAM_VALUE.SERIAL_WIDTH}
}

proc update_MODELPARAM_VALUE.TDATA_WIDTH { MODELPARAM_VALUE.TDATA_WIDTH PARAM_VALUE.TDATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.TDATA_WIDTH}] ${MODELPARAM_VALUE.TDATA_WIDTH}
}

proc update_MODELPARAM_VALUE.QC_COLLUMNS { MODELPARAM_VALUE.QC_COLLUMNS PARAM_VALUE.QC_COLLUMNS } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.QC_COLLUMNS}] ${MODELPARAM_VALUE.QC_COLLUMNS}
}

proc update_MODELPARAM_VALUE.QC_ROWS { MODELPARAM_VALUE.QC_ROWS PARAM_VALUE.QC_ROWS } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.QC_ROWS}] ${MODELPARAM_VALUE.QC_ROWS}
}

proc update_MODELPARAM_VALUE.NUMBER_RCE { MODELPARAM_VALUE.NUMBER_RCE PARAM_VALUE.NUMBER_RCE } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.NUMBER_RCE}] ${MODELPARAM_VALUE.NUMBER_RCE}
}

proc update_MODELPARAM_VALUE.N { MODELPARAM_VALUE.N PARAM_VALUE.N } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.N}] ${MODELPARAM_VALUE.N}
}

proc update_MODELPARAM_VALUE.K { MODELPARAM_VALUE.K PARAM_VALUE.K } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.K}] ${MODELPARAM_VALUE.K}
}

proc update_MODELPARAM_VALUE.ACCUM_END { MODELPARAM_VALUE.ACCUM_END PARAM_VALUE.ACCUM_END } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.ACCUM_END}] ${MODELPARAM_VALUE.ACCUM_END}
}


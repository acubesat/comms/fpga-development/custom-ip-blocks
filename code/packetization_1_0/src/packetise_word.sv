`timescale 1ns / 1ps

module Packetization #(

        // Slave port 0 parameters.
        parameter C_S00_AXIS_TDATA_WIDTH = 16,
        
        // Slave port 1 parameters. Associated with the FIR handling the quadrature component.
        parameter C_S01_AXIS_TDATA_WIDTH = 16,

        // Master port 0 parameters. Associated with the FIR handling the in-phase component.
        parameter C_M00_AXIS_TDATA_WIDTH = 32, 
        parameter C_M00_AXIS_TKEEP_WIDTH = C_M00_AXIS_TDATA_WIDTH/8
    )(

        // General inputs and outputs.
        input wire aclk,

        //AXI4-S slave port 0 inputs and outputs. 
        input wire s00_axis_tvalid,
        input wire [C_S00_AXIS_TDATA_WIDTH - 1 : 0] s00_axis_tdata,
        output logic s00_axis_tready,
        input wire s00_axis_tlast,
        
        //AXI4-S slave port 1 inputs and outputs.
        input wire s01_axis_tvalid,
        input wire [C_S01_AXIS_TDATA_WIDTH - 1 : 0] s01_axis_tdata,
        output logic s01_axis_tready,
        input wire s01_axis_tlast,

        //AXI4-S master port 0 inputs and outputs.
        output logic  m00_axis_tvalid,
        output logic [C_M00_AXIS_TDATA_WIDTH - 1 : 0] m00_axis_tdata,
        input wire m00_axis_tready,
        output logic [C_M00_AXIS_TKEEP_WIDTH - 1 : 0] m00_axis_tkeep,
        output logic m00_axis_tlast
    );

    localparam InPhaseSync = 2'b10;
    localparam QuadratureSync = 2'b01;
    localparam OffBit = 1'b0;

    always_comb begin
        m00_axis_tlast = s00_axis_tlast & s01_axis_tlast;
    end

    always_comb begin
        m00_axis_tvalid = s00_axis_tvalid;
    end

    always_comb begin
        s00_axis_tready = m00_axis_tready;
        s01_axis_tready = m00_axis_tready;
    end

    always_comb begin        
        m00_axis_tdata = {InPhaseSync, s00_axis_tdata[13:1], OffBit, QuadratureSync, s01_axis_tdata[13:1], OffBit};
    end

    always_comb begin
        m00_axis_tkeep = -1;
    end
endmodule
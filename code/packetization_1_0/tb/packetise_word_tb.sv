`timescale 1ns / 1ps
`include "src/packetise_word.sv"
module Packetization_tb;

    // Parameters
    localparam C_S00_AXIS_TDATA_WIDTH = 16;
    localparam C_S01_AXIS_TDATA_WIDTH = 16;
    localparam C_M00_AXIS_TDATA_WIDTH = 32;
    localparam C_M00_AXIS_TKEEP_WIDTH = C_M00_AXIS_TDATA_WIDTH / 8;

    // Testbench Signals
    reg s00_axis_tvalid;
    reg [C_S00_AXIS_TDATA_WIDTH - 1 : 0] s00_axis_tdata;
    wire s00_axis_tready;
    reg s00_axis_tlast;
    reg s01_axis_tvalid;
    reg [C_S01_AXIS_TDATA_WIDTH - 1 : 0] s01_axis_tdata;
    wire s01_axis_tready;
    reg s01_axis_tlast;
    wire m00_axis_tvalid;
    wire [C_M00_AXIS_TDATA_WIDTH - 1 : 0] m00_axis_tdata;
    reg m00_axis_tready;
    wire [C_M00_AXIS_TKEEP_WIDTH - 1 : 0] m00_axis_tkeep;
    wire m00_axis_tlast;

    // Instantiate the Unit Under Test (UUT)
    Packetization #(
        .C_S00_AXIS_TDATA_WIDTH(C_S00_AXIS_TDATA_WIDTH),
        .C_S01_AXIS_TDATA_WIDTH(C_S01_AXIS_TDATA_WIDTH),
        .C_M00_AXIS_TDATA_WIDTH(C_M00_AXIS_TDATA_WIDTH),
        .C_M00_AXIS_TKEEP_WIDTH(C_M00_AXIS_TKEEP_WIDTH)
    ) uut (
        .aclk(aclk),
        .s00_axis_tvalid(s00_axis_tvalid),
        .s00_axis_tdata(s00_axis_tdata),
        .s00_axis_tready(s00_axis_tready),
        .s00_axis_tlast(s00_axis_tlast),
        .s01_axis_tvalid(s01_axis_tvalid),
        .s01_axis_tdata(s01_axis_tdata),
        .s01_axis_tready(s01_axis_tready),
        .s01_axis_tlast(s01_axis_tlast),
        .m00_axis_tvalid(m00_axis_tvalid),
        .m00_axis_tdata(m00_axis_tdata),
        .m00_axis_tready(m00_axis_tready),
        .m00_axis_tkeep(m00_axis_tkeep),
        .m00_axis_tlast(m00_axis_tlast)
    );

    // Clock generation
    initial begin
        aclk = 0;
        forever #5 aclk = ~aclk; // 100MHz clock
    end

    // Test Sequence
    initial begin
        // Initialize Inputs
        aresetn = 0;
        s00_axis_tvalid = 0;
        s00_axis_tdata = 0;
        s00_axis_tlast = 0;
        s01_axis_tvalid = 0;
        s01_axis_tdata = 0;
        s01_axis_tlast = 0;
        m00_axis_tready = 1;

        // Initialize dumpfile and dumpvars
        $dumpfile("Packetization_tb.vcd");
        $dumpvars(0, Packetization_tb);

        // Wait for global reset to finish
        #100;
        aresetn = 1;
        #20;

        // Stimulus: Send data through both slave ports
        s00_axis_tvalid = 1;
        s01_axis_tvalid = 1;
        s00_axis_tdata = 16'hA5A5;
        s01_axis_tdata = 16'h5A5A;
        s00_axis_tlast = 1;
        s01_axis_tlast = 1;

        // Wait to observe the output
        #20;

        // Complete
        s00_axis_tvalid = 0;
        s01_axis_tvalid = 0;
        s00_axis_tlast = 0;
        s01_axis_tlast = 0;

        #100; // Wait some time to observe the output

        $finish; // End simulation
    end

endmodule

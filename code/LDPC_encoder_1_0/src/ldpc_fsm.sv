`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12/12/2023 11:33:03 AM
// Design Name: 
// Module Name: ldpc_fsm
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module ldpc_fsm#(
    parameter QC_COLLUMNS = 128, //128, 3           //#collumns of each QC parity mini arrays
    parameter QC_ROWS = 128,     //128, 4          //#rows of mini QC parity arrays 
    parameter NUMBER_RCE = 8,   //8,  2
    parameter N = 4096,           //4096, 8
    parameter K = 5210,           //5120, 10
    
    //gia qc_selector 
    parameter ACCUM_END = 128 //128, 4//otan teleivsei o end, mallon tha einai to 8
    )(
    //apo FSM ta inputs poy prepei kai poy einai gia verification
    input logic ACLOCK,
    input logic ARESET,          // Reset signal
    
    input logic out_done,        // Input signal for IN state
    input logic in_done,         // Input signal for SYST state
    output logic trigger_in,     // Output signal for IN state
    output logic trigger_out,    // Output signal for OUT state
    
    //for VERIFICATION toy FSM ta inouts
    output logic counter_codeword_data_fsm,     // counter_syst = 5120
    output logic intra_enable_data_fsm ,        // between datapath and fsm for enable                

    //for VERIFICATION toy FSM ta eswterika 
    output logic [1:0]current,
    output logic [1:0]next,
    
    //apo LDPC both ayta poy prepei kai ta verification 
    input logic serial_input,
    output logic serial_output        
    );
     
    //prosoxh diko moy module tryyyyy
         
    wire counter_syst_data_fsm;    
    wire intra_enable_accum_data_fsm;
    FSM fsm_d (                   // FSM module
        .ACLOCK(ACLOCK),
        .ARESET(ARESET),
        .out_done(out_done),
        .in_done(in_done),
        .code_syst(counter_syst_data_fsm),
        .code_output(counter_codeword_data_fsm),
        .enable(intra_enable_data_fsm),
        .enable_accum(intra_enable_accum_data_fsm),
        .trigger_in(trigger_in),
        .trigger_out(trigger_out),
        //GIA VERIFICATION toy fsm eswterika 
        .current(current),
        .next(next)
    );
  
    ldpc_top#(QC_COLLUMNS, QC_ROWS, NUMBER_RCE, N, K, ACCUM_END) ldpc_top_d(
        .ACLOCK(ACLOCK),
        .reset(ARESET),
        .enable(intra_enable_data_fsm),
        .enable_accum(intra_enable_accum_data_fsm),
        .serial_input(serial_input),
        .serial_output(serial_output),
        .counter_syst(counter_syst_data_fsm),
        .counter_codeword (counter_codeword_data_fsm )
    );
    
endmodule

`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/07/2024 04:30:35 PM
// Design Name: 
// Module Name: axi_master_fsm
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
module axis_master_fsm(
    input logic ACLOCK,
    input logic ARESETN,          // Reset signal negative high
    output logic axis_mVALID,
    input logic axis_mREADY,    
    output logic wr_en,
    output logic rd_en,
    output logic start_over,
    input logic sp_fifo_empty,
    input logic sp_fifo_full,
    input logic trigger_out,     //encoder starts giving out data //syst = 1
    output logic out_done
    );
    
    // 3 states
    typedef enum logic [1:0] {
        IDLE, RECEIVE_MODE, PUSHING_OUT
    } state;
    state current_state, next_state;  
    assign current = current_state; //to undo verification
    assign next = next_state;       //to undo verification
    
    // 1st always for reset state
    always_ff @(posedge ACLOCK or negedge ARESETN) begin: STATE_MEMORY
        if (!ARESETN)
            current_state <= IDLE; // Initial state is IDLE
        else
            current_state <= next_state; // Move to next state
    end
    
    //2nd always for next state
    always_comb begin: NEXT_STATE_LOGIC
        unique case(current_state)
            IDLE : next_state = (trigger_out) ? RECEIVE_MODE : IDLE;
            RECEIVE_MODE : next_state = (sp_fifo_full) ? PUSHING_OUT : RECEIVE_MODE;
            PUSHING_OUT : next_state = (sp_fifo_empty) ? IDLE : PUSHING_OUT;
            default: next_state = IDLE;
        endcase
    
   end         
            
  // 3rd final always for 
  always_comb begin: OUTPUT_LOGIC
    unique case (current_state)
      IDLE: begin
        wr_en = 0;
        rd_en = 0;
        start_over = 1;
        axis_mVALID = 0;
        out_done = 1;
      end
      RECEIVE_MODE: begin
            wr_en = 1;
            rd_en = 0;
            start_over = 0;
            out_done = 0;
      end
      PUSHING_OUT: begin
          axis_mVALID = 1;
          wr_en = 0;
          start_over = 0;
          if ( axis_mREADY ) begin  // normally its: "if (axis_mREADY && axis_mVALID) begin" but axis_mVALID is always 1. For verif expression coverage.
              rd_en = 1;
          end 
          else begin 
              rd_en = 0;
          end
      end
    endcase
  end
endmodule

`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/02/2024 06:05:17 PM
// Design Name: 
// Module Name: axi_slave
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module axi_slave#(      
    // Parameters mainly for ps_fifo
    parameter WIDTHA = 64,      //4,     //64      //input (write)
    parameter DEPTHA = 80,      //3,      //80;
    parameter WIDTHB = 64,      //1,      //64   //output (read)
    parameter DEPTHB = 5120     //12      //5120;
    )(
    //ports
    input logic ACLOCK,
    input logic ARESETN,            //active low panta, mhn ta ksanaleme
    input logic S_AXIS_TVALID,
    output logic S_AXIS_TREADY,
    input logic [WIDTHA-1:0]S_AXIS_TDATA,  
    input logic trigger_in,       
    output logic in_done,
    output logic [WIDTHB-1:0] out_data    //to be read data
    );
 
    logic intra_wr_en;
    wire intra_rd_en;
    logic intra_fifo_full;
    wire intra_ps_fifo_empty;
    wire intra_start_over;
    
    ps_fifo #(WIDTHA, DEPTHA, WIDTHB, DEPTHB) ps_fifo_d(
        .clock(ACLOCK),
        .reset(ARESETN),              //active low panta, mhn ta ksanaleme
        .wr_en(intra_wr_en),              //otan theloyn na grapsoyn
        .rd_en(intra_rd_en),              //otan theloyn na diabasoyn
        .start_over(intra_start_over),
        .in_data(S_AXIS_TDATA),      //to be stored/writen data
        .out_data(out_data),    //to be read data
        .ps_fifo_empty(intra_ps_fifo_empty),
        .ps_fifo_full(intra_fifo_full)
    );
    
    axis_slave_fsm axis_slave_fsm_d (
        .ACLOCK(ACLOCK),
        .ARESETN(ARESETN),  
        .axis_sVALID(S_AXIS_TVALID),
        .axis_sREADY(S_AXIS_TREADY),
        .wr_en(intra_wr_en),
        .rd_en(intra_rd_en),
        .start_over(intra_start_over),
        .ps_fifo_empty(intra_ps_fifo_empty),
        .ps_fifo_full(intra_fifo_full),
        .trigger_in(trigger_in),
        .in_done(in_done)
    );    
    
endmodule

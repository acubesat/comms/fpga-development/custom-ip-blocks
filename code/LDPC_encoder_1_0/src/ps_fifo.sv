`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12/29/2023 02:15:50 PM
// Design Name: 
// Module Name: sp_fifo
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
module ps_fifo #(
    // Parameters
    parameter WIDTHA = 64,      //4,     //64      //input (write)
    parameter DEPTHA = 80,      //3,      //80;
    parameter WIDTHB = 64,      //1,      //64   //output (read)
    parameter DEPTHB = 5120     //12      //5120;
    )( // 8 for verification // Depth of the module accum
    //inouts
    input logic clock,
    input logic reset,              //active low panta, mhn ta ksanaleme
    input logic wr_en,              //otan theloyn na grapsoyn
    input logic rd_en,              //otan theloyn na diabasoyn
    input logic start_over,         //reset pointers
    input logic [WIDTHA-1:0] in_data,      //to be stored/writen data
    output logic [WIDTHB-1:0] out_data,    //to be read data
    output logic ps_fifo_empty,
    output logic ps_fifo_full
);
    localparam ADDRWIDTHA = $clog2(DEPTHA);
    localparam ADDRWIDTHB = $clog2(DEPTHB);
    
    reg [ADDRWIDTHA-1 +1:0] write_ptr; ////ebala to +1 !!!!!!!!!
    reg [ADDRWIDTHB-1 +1:0] read_ptr;     ////ebala to +1 !!!!!!!!!     

    // Flags
    reg empty_flag;     //internal gia otan adeiasei
    reg full_flag;      //internal gia otan gemisei
    
    // Our bram 
    ps_fifo_bram #(WIDTHA, DEPTHA, WIDTHB, DEPTHB) ps_fifo_bram_d(
        .clk(clock), 
        .enaA(wr_en), //em afoy ta exw balei idia logiko einai 
        .weA(wr_en), 
        .enaB(rd_en), 
        .addrA(write_ptr), 
        .addrB(read_ptr), 
        .diA(in_data), 
        .doB(out_data)
    );

    // Write logic
    always_ff @(posedge clock or negedge reset) begin
        if (!reset) begin
            write_ptr <= 0;
        end else if (wr_en && !full_flag) begin
            write_ptr <= write_ptr + 1;
        end else if (start_over) begin
            write_ptr <= 0;      
        end
    end
    

    // Read logic
    always_ff @(posedge clock or negedge reset) begin
        if (!reset) begin
            read_ptr <= 0;
        end else if (rd_en && !empty_flag) begin
            read_ptr <= read_ptr + 1;
        end else if (start_over) begin
                read_ptr <= 0;    
        end
    end
    
    // Empty and Full flags logic
    always_ff @(posedge clock) begin
        empty_flag = (write_ptr == 0) || (read_ptr == DEPTHB -1 ) || (read_ptr == DEPTHB );     //dialejh 7 sel 25 apo pshfiaka 2        
        full_flag =  (write_ptr >= DEPTHA-1);  //(write_ptr == (DEPTHA -1) -1 && wr_en);    //allaksa apo == se >=
    end
    // Flags
    assign ps_fifo_empty = empty_flag;
    assign ps_fifo_full = full_flag;
    
endmodule

/*
//ayto htan prin xwris to bram 
module ps_fifo #(
    // Parameters
    parameter DEPTH = 8, // Depth of the module accum
    parameter DATA_WIDTH = 4 //size of axi interface TDATA64
    )(
    //inouts
    input logic clock,
    input logic reset,              //active low panta, mhn ta ksanaleme
    input logic wr_en,              //otan theloyn na grapsoyn
    input logic rd_en,              //otan theloyn na diabasoyn
    input logic [DATA_WIDTH-1:0] in_data,      //to be stored/writen data
    output logic [0:0] out_data,    //to be read data
    output logic ps_fifo_empty,
    output logic ps_fifo_full
);

    // Internal FIFO memory
    reg [0:0] fifo_mem [DEPTH-1:0];     //sos na dwsw prosoxh   (* ram_style = "distributed" *) reg [data_size-1:0] myram [2**addr_size-1:0];
    
    reg [2:0] read_ptr = 0;         // SOS SOS SOS SOS KANONIKA THELEI CLOG2
    reg [2:0] write_ptr = 0;

    // Flags
    reg empty_flag;     //internal gia otan adeiasei
    reg full_flag;      //internal gia otan gemisei

    // Data registers
    reg [0:0] data_out_reg;     

    int i = 0;


    // Write logic
    always_ff @(posedge clock or negedge reset) begin
        if (!reset) begin
            write_ptr <= 4'b0000;
        end else if (wr_en && !full_flag) begin
            for(i = 0; i <= DATA_WIDTH-1; i++)begin
                fifo_mem[write_ptr+i] = in_data[DATA_WIDTH -1 - i];
            end
            write_ptr = write_ptr + DATA_WIDTH;
        end
    end

    // Read logic
    always_ff @(posedge clock or negedge reset) begin
        if (!reset) begin
            read_ptr <= 4'b0000;
        end else if (rd_en && !empty_flag) begin
            data_out_reg <= fifo_mem[read_ptr];
            read_ptr <= read_ptr + 1;
        end
    end

    // Output data
    assign out_data = data_out_reg;


    // Empty and Full flags logic
    always_ff @(posedge clock) begin
        empty_flag = (write_ptr == 0);
        full_flag = ((write_ptr-1) ==(DEPTH -1) );
        //full_flag = (((write_ptr +DATA_WIDTH) == DEPTH) && wr_en);

        //empty_flag = (write_ptr == (read_ptr +1) && rd_en);     //dialejh 7 sel 25 apo pshfiaka 2        
        //full_flag =  (read_ptr == (write_ptr +DATA_WIDTH) && wr_en);
    end
        
    // Flags
    assign ps_fifo_empty = empty_flag;
    assign ps_fifo_full = full_flag;

endmodule
*/
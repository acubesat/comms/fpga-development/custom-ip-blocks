`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/15/2024 11:53:03 PM
// Design Name: 
// Module Name: dlatch
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module dlatch(
  input wire D,
  input wire enable,
  input wire reset,
  output reg Q
);
  always @(D or enable or reset) begin
    if(reset == 1)begin
        if (enable)
          Q <= D;
        else
          Q <= 0;
    end
    else
        Q <= 0;  
  end
 
endmodule

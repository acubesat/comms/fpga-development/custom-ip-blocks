`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/01/2024 11:34:50 AM
// Design Name: 
// Module Name: ldpc_2
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module ldpc_2#(
    // Parameters for axi slaves
    parameter SERIAL_WIDTH = 1,
    parameter TDATA_WIDTH = 64,
    //for ldpc main encoder
    parameter QC_COLLUMNS = 128, //3, //128,            //#collumns of each QC parity mini arrays
    parameter QC_ROWS = 128, //4,     //128          //#rows of mini QC parity arrays 
    parameter NUMBER_RCE = 8, //2,   //8
    parameter N = 4096, //8,           //4096,
    parameter K = 5120, //10,           //5120
    parameter ACCUM_END = 8 //4 //otan teleivsei o end, mallon tha einai to 8
    )(
    input logic ACLOCK,
    input logic ARESETN,
    //slave interface
    input logic axis_sVALID,
    output logic axis_sREADY,
    input logic [TDATA_WIDTH-1:0]axis_sTDATA,
    //master interface
    output logic axis_mVALID,
    input logic axis_mREADY,
    output logic [TDATA_WIDTH-1:0]axis_mTDATA
    );
    
    localparam SLAVE_DEPTH = N/TDATA_WIDTH;
    localparam MASTER_DEPTH = K/TDATA_WIDTH;
    
    wire intra_axis_sdata_main;    //connects axis_slave_interface output signal serial tdata with ldpc_fsm serial input
    wire intra_main_axis_mdata;    //connects ldpc_fsm serial output with axis_slave_interface intput signal serial tdata
    wire intra_in_done;
    wire intra_out_done;
    wire rd_en_slave;
    wire intra_trigger_out ;
    wire intra_trigger_in;
    wire intra_enable_data_fsm;
    
    //axi4stream slave
    axi_slave#(TDATA_WIDTH, SLAVE_DEPTH, SERIAL_WIDTH , N) axi_slave_dut(
        .ACLOCK(ACLOCK),
        .ARESETN(ARESETN),            //active low panta, mhn ta ksanaleme
        .S_AXIS_TVALID(axis_sVALID),
        .S_AXIS_TREADY(axis_sREADY),
        .S_AXIS_TDATA(axis_sTDATA),
        .trigger_in(intra_trigger_in),       
        .in_done(intra_in_done),
        .out_data(intra_axis_sdata_main)   //to be read data
    );

    //ldpc_fsm main encoder
    ldpc_fsm#(QC_COLLUMNS, QC_ROWS, NUMBER_RCE, N, K, ACCUM_END) ldpc_fsm_d(
        //apo FSM ta inputs poy prepei kai poy einai gia verification
        .ACLOCK(ACLOCK),
        .ARESET(ARESETN),          // Reset signal
        //for VERIFICATION   toy FSM ta inouts
        .out_done(intra_out_done),        // Input signal for IN state
        .in_done(intra_in_done),         // Input signal for SYST state
        .counter_codeword_data_fsm(counter_codeword_data_fsm),// counter_syst = 5120
        .trigger_in(intra_trigger_in),     // Output signal for IN state
        .trigger_out(intra_trigger_out),    // Output signal for OUT state
        //for VERIFICATION  toy FSM ta eswterika 
        .current(current),
        .next(next),
        //apo LDPC both ayta poy prepei kai ta verification 
        .serial_input(intra_axis_sdata_main),
        .serial_output(intra_main_axis_mdata),          
        .intra_enable_data_fsm(intra_enable_data_fsm)
    );
    
    //axi4stream master
    axi_master#( SERIAL_WIDTH, K, TDATA_WIDTH, MASTER_DEPTH) axi_master_d(      
        .ACLOCK(ACLOCK),
        .ARESETN(ARESETN),            //active low panta, mhn ta ksanaleme
        .S_AXIS_TVALID(axis_mVALID),
        .S_AXIS_TREADY(axis_mREADY),
        .S_AXIS_TDATA(axis_mTDATA),
        .trigger_out(intra_trigger_out),       
        .out_done(intra_out_done),
        .writen_data(intra_main_axis_mdata)        //to be writen data
    );

endmodule
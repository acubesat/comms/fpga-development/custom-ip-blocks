`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/05/2024 05:24:49 PM
// Design Name: 
// Module Name: sp_fifo_bram
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: Asymmetric port RAM. Read Wider than Write. Read Statement in loop
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
module ps_fifo_bram (clk, enaA, weA, enaB, addrA, addrB, diA, doB);
parameter WIDTHA = 64;      //4;      //64         //input write
parameter DEPTHA = 80;      //3;      //80;
//parameter ADDRWIDTHA = $clog2(DEPTHA);

parameter WIDTHB = 64;      //1;     //64          //output read
parameter DEPTHB = 5120;    //12;  //5120;
//parameter ADDRWIDTHB = $clog2(DEPTHB);

localparam ADDRWIDTHA = $clog2(DEPTHA); 
localparam ADDRWIDTHB = $clog2(DEPTHB);

input clk;
input weA;
input enaA, enaB;
input [ADDRWIDTHA-1:0] addrA;
input [ADDRWIDTHB-1:0] addrB;
input [WIDTHA-1:0] diA;
output [WIDTHB-1:0] doB;
`define max(a,b) {(a) > (b) ? (a) : (b)}
`define min(a,b) {(a) < (b) ? (a) : (b)}

localparam maxSIZE = `max(DEPTHA, DEPTHB);
localparam maxWIDTH = `max(WIDTHA, WIDTHB);
localparam minWIDTH = `min(WIDTHA, WIDTHB);

localparam RATIO = maxWIDTH / minWIDTH;
localparam log2RATIO = $clog2(RATIO);
reg [log2RATIO-1:0] lsbaddr; //u ll understand later

reg [minWIDTH-1:0] RAM [0:maxSIZE-1];
reg [WIDTHB-1:0] readB;

always @(posedge clk) begin
    integer i;
    //kanonika einai etsi alla egw tha to grapsw etsi 
    //if (enaA) begin
    //    if (weA)
    if(enaA && weA) begin
            for (i = 0; i < RATIO; i = i+1) begin
                lsbaddr = i;                            //na dokimasw na to bgalw kai katw na balw sketo i 
                RAM[{addrA, lsbaddr}] <= diA[(RATIO-i)*minWIDTH-1 -: minWIDTH]; //allaksa from official vivado to store values in reverse
            end
        end
    end

always @(posedge clk) begin : ramread
    if (enaB) begin
        readB <= RAM[addrB];
    end
    ///////////////////////to bazw egwww prosoxh an einai bram 
    else begin
        readB <= 0;
    end 
    ////////////////////// to ebala egw prpsxh mhpws to bgalv
end
assign doB = readB;

endmodule


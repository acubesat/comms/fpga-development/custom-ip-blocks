`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03/12/2024 08:15:08 PM
// Design Name: 
// Module Name: verif_ff
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module verif_ff(
    input logic ACLOCK,
    input logic ARESETN,
    //master encoders interface
    input logic axis_mVALID,
    input logic axis_mREADY,
    input logic [64-1:0]axis_mTDATA
    );
    
    logic [64-1:0]inside_axis_mTDATA;
    
    always_ff@( posedge ACLOCK ) begin
        if(axis_mVALID && axis_mREADY ) begin
            inside_axis_mTDATA = axis_mTDATA;
        end
    end
endmodule

`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12/16/2023 08:56:02 PM
// Design Name: 
// Module Name: test
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

/*
module test#(
    WIDTH = 8,
    TIMES = 3,
    )(
    input clock,
    input reset,
    input enable,
    output out
    );
    logic intra_count_demux;
    
    counter #(TIMES) counter_dut(
    .clock(clock),
    .enable(enable),
    .reset(reset),
    .ch_mode(intra_count_demux)
    );
    
    demux_modif#() demux_modif_dut(
    
    );
endmodule
*/
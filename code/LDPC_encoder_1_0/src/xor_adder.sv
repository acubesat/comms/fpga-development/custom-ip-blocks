`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/15/2023 01:05:47 AM
// Design Name: 
// Module Name: xor_adder
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:    IT WORKS as expected, combinational
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module xor_adder(
    input logic in_bit_1,
    input logic in_bit_2,
    input logic reset,
    output logic out_bit
    );
    
    // Combinational logic for adding input bitstreams
    always_comb begin 
    if(reset)
        out_bit <= in_bit_1 ^ in_bit_2;
    else 
        out_bit <= 0;
    end

endmodule

`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/23/2023 02:20:06 AM
// Design Name: 
// Module Name: DFF
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
module DFF #(
    parameter SIZE = 4
    )(
    input logic reset,      //active low
    input  wire [SIZE-1:0] D,     // Data input
    input  wire  clock,   // Clock input
    input logic enable,
    output reg [SIZE-1:0] Q      // Output
);
    always @(posedge clock or negedge reset) begin
        if(!reset)
            Q <= 0;    
        else if (enable)
            Q <= D;       // Q takes the value of D on the rising edge of the clock
        else 
            Q <= 0;
    end
endmodule

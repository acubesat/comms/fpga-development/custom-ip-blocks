`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12/29/2023 02:15:50 PM
// Design Name: 
// Module Name: sp_fifo
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
module sp_fifo #(
    // Parameters
    parameter WIDTHA = 1,              //input (write)
    parameter DEPTHA = 5120,      //12 5120;
    parameter WIDTHB = 64,     //4  64          //output (read)
    parameter DEPTHB = 80     //3   80;
    )( // 8 for verification // Depth of the module accum
    //inouts
    input logic clock,
    input logic reset,              //active low panta, mhn ta ksanaleme
    input logic wr_en,              //otan theloyn na grapsoyn
    input logic rd_en,              //otan theloyn na diabasoyn
    input logic start_over,
    input logic [WIDTHA-1:0] in_data,      //to be stored/writen data
    output logic [WIDTHB-1:0] out_data,    //to be read data
    output logic sp_fifo_empty,
    output logic sp_fifo_full
);
    localparam ADDRWIDTHA = $clog2(DEPTHA);
    localparam ADDRWIDTHB = $clog2(DEPTHB);
    
    reg [ADDRWIDTHA-1:0] write_ptr;
    reg [ADDRWIDTHB-1:0] read_ptr;

    // Flags
    reg empty_flag;     //internal gia otan adeiasei
    reg full_flag;      //internal gia otan gemisei
    
    // Our bram 
    sp_fifo_bram #(WIDTHA, DEPTHA, WIDTHB, DEPTHB) sp_fifo_bram_d(
        .clk(clock), 
        .enaA(wr_en), 
        .weA(wr_en), 
        .enaB(rd_en), 
        .addrA(write_ptr), 
        .addrB(read_ptr), 
        .diA(in_data), 
        .doB(out_data)
    );

    // Write logic
    always_ff @(posedge clock or negedge reset) begin
        if (!reset) begin
            write_ptr <= 0;
        end else if (wr_en && !full_flag) begin
            write_ptr <= write_ptr + 1;
        end
        else if (start_over) begin 
            write_ptr <= 0;
        end 
    end

    // Read logic
    always_ff @(posedge clock or negedge reset) begin
        if (!reset) begin
            read_ptr <= 0;
        end else if (rd_en && !empty_flag) begin
            read_ptr <= read_ptr + 1;
        end
        else if (start_over) begin 
            read_ptr <= 0;
        end 
    end
    
    // Empty and Full flags logic
    always_ff @(posedge clock) begin
        empty_flag = (write_ptr == 0) || ((read_ptr == DEPTHB -2) && rd_en);     //dialejh 7 sel 25 apo pshfiaka 2       //allaksa prin htan empty_flag = (write_ptr == 0) || (read_ptr == DEPTHB -1);  
        if ((write_ptr == (DEPTHA -1) -1 && wr_en)) begin
            full_flag =  1;
        end
        else if(write_ptr == (DEPTHA -1))
            full_flag = 1;
        else 
            full_flag = 0;
    end
    // Flags
    assign sp_fifo_empty = empty_flag;
    assign sp_fifo_full = full_flag;
    
endmodule


/*
//ayto to eixa kanei prin. Den exei bram opote den voleyei sthn periptvwsh ayth parolo poy leitoyrgei plhrws sto behavioral simulation

module sp_fifo #(
    // Parameters
    parameter DEPTH = 4096)( // 8 for verification // Depth of the module accum
    //inouts
    input logic clock,
    input logic reset,              //active low panta, mhn ta ksanaleme
    input logic wr_en,              //otan theloyn na grapsoyn
    input logic rd_en,              //otan theloyn na diabasoyn
    input logic [0:0] in_data,      //to be stored/writen data
    output logic [63:0] out_data,    //to be read data
    output logic sp_fifo_empty,
    output logic sp_fifo_full
);

    // Internal FIFO memory
    reg [0:0] fifo_mem [DEPTH-1:0];     //sos na dwsw prosoxh   (* ram_style = "distributed" *) reg [data_size-1:0] myram [2**addr_size-1:0];
    
    reg [11:0] read_ptr = 4'b0000;
    reg [11:0] write_ptr = 4'b0000;

    // Flags
    reg empty_flag;     //internal gia otan adeiasei
    reg full_flag;      //internal gia otan gemisei

    // Data registers
    reg [3:0] data_out_reg;     

    // Write logic
    always_ff @(posedge clock or negedge reset) begin
        if (!reset) begin
            write_ptr <= 4'b0000;
        end else if (wr_en && !full_flag) begin
            fifo_mem[write_ptr] <= in_data;
            write_ptr <= write_ptr + 1;
        end
    end

    // Read logic
    always_ff @(posedge clock or negedge reset) begin
        if (!reset) begin
            read_ptr <= 4'b0000;
        end else if (rd_en && !empty_flag) begin
            data_out_reg <= {fifo_mem[read_ptr], fifo_mem[read_ptr + 1], fifo_mem[read_ptr + 2], fifo_mem[read_ptr + 3]};
            read_ptr <= read_ptr + 4;
        end
    end

    // Output data
    assign out_data = data_out_reg;


    // Empty and Full flags logic
    always_ff @(posedge clock) begin
        empty_flag = (write_ptr == (read_ptr +4) && rd_en);     //dialejh 7 sel 25 apo pshfiaka 2        
        full_flag =  (read_ptr == (write_ptr +1) && wr_en);
    end
        
    // Flags
    assign sp_fifo_empty = empty_flag;
    assign sp_fifo_full = full_flag;

endmodule
*/
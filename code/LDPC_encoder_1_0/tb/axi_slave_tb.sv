`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/02/2024 11:07:56 PM
// Design Name: 
// Module Name: axi_slave_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
module axi_slave_tb;
    // Parameters
    localparam WIDTHA = 4;      //64      //input (write)
    localparam DEPTHA = 3;      //80;
    localparam WIDTHB = 1;      //64   //output (read)
    localparam DEPTHB = 12;      //5120;  
    //ports
    logic ACLOCK;
    logic ARESETN;            //active low panta, mhn ta ksanaleme
    logic S_AXIS_TVALID;
    logic S_AXIS_TREADY;
    logic [WIDTHA-1:0]S_AXIS_TDATA;
    logic trigger_in;      
    logic in_done;
    logic [WIDTHB-1:0] out_data;    //to be read data
    
    axi_slave#(WIDTHA, DEPTHA, WIDTHB, DEPTHB) axi_slave_dut(
        //ports
        .ACLOCK(ACLOCK),
        .ARESETN(ARESETN),            //active low panta, mhn ta ksanaleme
        .S_AXIS_TVALID(S_AXIS_TVALID),
        .S_AXIS_TREADY(S_AXIS_TREADY),
        .S_AXIS_TDATA(S_AXIS_TDATA),
        .trigger_in(trigger_in),       
        .in_done(in_done),
        .out_data(out_data)    //to be read data
        );

    // Clock generation
    always #5 ACLOCK = ~ACLOCK;

    // Test procedure
    initial begin
        ACLOCK = 1;
        trigger_in = 0;
        S_AXIS_TVALID = 0;
        // Reset
        ARESETN = 0;
        #14;
        ARESETN = 1;

        #10;        //24ns          sta 24ns stelnei o master valid kai data 
        S_AXIS_TVALID = 1;
        S_AXIS_TDATA = 4'd1;     //1o
        #10;
        S_AXIS_TDATA = 4'd2;     //sto 54 ns prepei na paralabei to prwto packet kai sta 64 prepei logika to 2o packet
        #10;
        S_AXIS_TDATA = 4'd3;     //sta 74 ns prepei logika to 3o packet mazi kai peftei to ready 
        #10;
        trigger_in = 1;
        
        S_AXIS_TVALID = 0;
        #30;                    //tpt gia 3 kykloys
        #120;                   //tha eprepe na bgoyn me th seira oi 12 (3x4) arithmoi seiriaka         
        //round 2 of verification first ready then valid
        S_AXIS_TVALID = 0;
        S_AXIS_TDATA = 4'd3;
        trigger_in = 1;
        #30;
        S_AXIS_TVALID = 1;
        #10;
        S_AXIS_TDATA = 4'd4;
        #10;
        S_AXIS_TDATA = 4'd5;
        trigger_in = 1;
        #50;
        #100;

        // End simulation
        $finish;
    end
    


endmodule

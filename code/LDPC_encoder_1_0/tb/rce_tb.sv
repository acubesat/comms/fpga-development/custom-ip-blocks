`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/15/2023 12:40:27 AM
// Design Name: 
// Module Name: rce_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module rce_tb;
    localparam QC_COLLUMNS = 3;          //#collumns of each QC parity mini arrays
    //localparam QC_ROWS = 3;              //#rows of mini QC parity arrays 
    
    logic [QC_COLLUMNS-1:0] row_values;  //QC_COLLUMNS values of first row of each QC array ;  //m values of first row of each parity module accum 
    logic reset;    
    logic enable;
    logic enable_dff;       //wire that enable specifficaly dffs above clock
    logic clock;
    logic msg_in;      //1 bit serial
    logic rce_in;      //1 bit serial
    logic system_par;
    logic rce_out;    //1bit
 
     //rce #(QC_COLLUMNS, QC_ROWS) rce_dut(
    rce #(QC_COLLUMNS) rce_dut(
        .row_values(row_values),
        .enable(enable),
        .enable_dff(enable_dff),
        .rce_in(rce_in),
        .rce_out(rce_out),
        .clock(clock ),
        .reset(reset),
        .msg_in(msg_in),
        .system_par(system_par)
    );
    
    always #5 clock = ~clock; 
    // Test stimulus
    initial begin
    clock = 1'b1;
    reset = 1'b0;
    row_values = 3'b100;
    rce_in = 1'b1;
    msg_in =0;
    system_par = 0;
    enable = 0;
    enable_dff =0;
    #10;
    enable_dff =1;
    #3 
    reset = 1;
    msg_in = 1'b1;
    enable = 1;
    #7; 
    msg_in = 1'b1;
    #10;
    msg_in = 1'b1;
    #10 //3h fora clock
    row_values = 3'b001;
    msg_in = 1'b1;    
    #10
    msg_in = 1'b1;
    #10
    msg_in = 1'b1;
    #10;
    enable_dff =0;
    system_par = 1;
    
    #60 // Wait for some time
    $finish;
  end
    
endmodule

`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/02/2024 11:07:56 PM
// Design Name: 
// Module Name: axi_slave_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
module axi_master_tb;
    // Parameters
    localparam WIDTHA = 1;              //input (write)
    localparam DEPTHA = 12;      //5120;
    localparam WIDTHB = 4;     //64          //output (read)
    localparam DEPTHB = 3;      //80;
    //ports
    logic ACLOCK;
    logic ARESETN;            //active low panta, mhn ta ksanaleme
    logic S_AXIS_TVALID;
    logic S_AXIS_TREADY;
    logic [WIDTHB-1:0]S_AXIS_TDATA;
    logic trigger_out;      
    logic out_done;
    logic [WIDTHA-1:0] writen_data;    //to be writen data

    axi_master#( WIDTHA, DEPTHA, WIDTHB, DEPTHB) axi_master_dut(      
        .ACLOCK(ACLOCK),
        .ARESETN(ARESETN),            //active low panta, mhn ta ksanaleme
        .S_AXIS_TVALID(S_AXIS_TVALID),
        .S_AXIS_TREADY(S_AXIS_TREADY),
        .S_AXIS_TDATA(S_AXIS_TDATA),
        .trigger_out(trigger_out),       
        .out_done(out_done),
        .writen_data(writen_data)        //to be writen data
        );


    // Clock generation
    always #5 ACLOCK = ~ACLOCK;

    // Test procedure
    initial begin
        ACLOCK = 1;
        trigger_out = 0;
        S_AXIS_TREADY = 0;
        // Reset
        ARESETN = 0;
        #14;
        ARESETN = 1;

        #10;        //@ 24ns ksekina na dinetai adeia gia na mpoyn ta dedomena mesa sto sp_fifo 
        trigger_out = 1;
        #10;        
        writen_data = 1;      
        #10;                    // sta 34ns mpainei mesa sto sp_fifo  0
        writen_data = 0;
        #10;                    // sto 44 0
        #10;                    // sto 54 0
        #10;                    // sto 64 ksana 0 ara synolo 1000 = 'd8;
        writen_data = 1;        //sto 74 mpainei 1 
        #40;                    //omoia sto 84, 94
        writen_data = 0;        //sto 104 0 ara, 1110 = 'd15
        #10;
        writen_data = 1;        //sto 114 mpainei 1 
        #20;                    //omoia sto 124, 134
        writen_data = 1;        //sto 144 0 ara, 1111 = 'd16
        #10;       
        S_AXIS_TREADY = 1;
        trigger_out = 0;
        #30;                    //tpt gia 3 kykloys


        #40;                //restart 
        trigger_out = 1;
        writen_data = 0;
        #30;
        writen_data = 1;
        #10;
        writen_data = 0;
        #10;
        writen_data = 1;
        #10;
        writen_data = 0;
        #50;
        #20;
        S_AXIS_TREADY = 1;
        #40;
        
        // End simulation
        $finish;
    end
    


endmodule

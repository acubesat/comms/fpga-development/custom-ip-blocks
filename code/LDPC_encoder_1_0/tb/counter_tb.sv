`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/14/2023 12:54:24 AM
// Design Name: 
// Module Name: counter_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments: Me ayto to script ginetai high ena output signal me name:ch_mode kathe stigmh 0. Ksekin;ontas apo to epomeno clock akribws sthn 12h(TIMES) fora tha ksanaginei high. 
// 
//////////////////////////////////////////////////////////////////////////////////
module counter_tb;
  // Parameters
  localparam TIMES = 4;
  // Inputs
  logic clock;
  logic reset;
  logic enable;
  // Outputs
  logic ch_mode;

  // Instantiate the Counter module
  counter #(TIMES) dut (
    .clock(clock),
    .reset(reset),
    .enable(enable),
    .ch_mode(ch_mode)
  );

  // Clock generation
  initial begin
    clock = 0;
    forever #5 clock = ~clock;
  end

  initial begin
    reset = 0;
    enable = 0;
    #9;
    reset = 1;
    enable = 1;
      
    #90
    $finish;
  end

endmodule


/*
module counter_tb;
  // Parameters
  localparam TIMES = 12;
  // Inputs
  logic clock;
  // Outputs
  logic ch_mode;
  logic [$clog2(TIMES):0] count;

  // Instantiate the Counter module
  counter_different  #(TIMES) dut (
    .clock(clock),
    .ch_mode(ch_mode),
    .count(count)
  );

  // Clock generation
  initial begin
    clock = 0;
    forever #5 clock = ~clock;
  end

  initial begin
  
    #360
    $finish;
  end

endmodule
*/
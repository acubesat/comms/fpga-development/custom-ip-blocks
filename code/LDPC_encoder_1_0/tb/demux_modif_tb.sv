`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12/14/2023 08:34:28 PM
// Design Name: 
// Module Name: demux_modif_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module demux_modif_tb;
  localparam INPUTS = 8;
  localparam SELECT = 4;

  logic [INPUTS-1:0]in;
  logic [SELECT-1:0] sel;
  logic enable, reset;
  logic out;

  // Instantiate demux module
  demux_modif #(INPUTS, SELECT) dut (
    .in(in),
    .sel(sel),
    .enable(enable),
    .reset(reset),
    .out(out)
  );

  initial begin
    // Initialize inputs
    in = 8'b01010101;
    sel = 'b0;
    enable = 0;
    reset = 1;

    // Apply test cases

    // Test case 1: Enable demux and select input 3
    enable = 1;
    sel = 2'b00;
    #10;
    if (out !== 1'b1) $fatal("Test case 1 failed!");
    
    #10;
    sel = 2'b01;
    
    #10;
    sel = 2'b10;
    
    #10;
    sel = 2'b11;
    
    #10;
    sel = 3'b100;
    
    #10;
    sel = 3'b101;
    #10;

    
    // Test case 2: Disable demux
    enable = 0;
    #10;
    if (out !== 1'b0) $fatal("Test case 2 failed!");

    // Test case 3: Reset demux
    reset = 1;
    #10;
    reset = 0;
    #10;
    if (out !== 1'b0) $fatal("Test case 3 failed!");

    // Add more test cases as needed

    // End simulation
    $finish;
  end

endmodule


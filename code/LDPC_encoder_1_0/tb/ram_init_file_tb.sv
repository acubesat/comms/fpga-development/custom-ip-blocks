`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/08/2024 01:23:22 PM
// Design Name: 
// Module Name: ram_init_file_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
/*
module ram_init_file_tb;
    localparam WIDTH = 1024;
    localparam DEPTH = 32;
    localparam ADDR = $clog2(DEPTH);
    
    logic clk;
    logic rd_en;
    logic [ADDR-1:0] addr;
    logic [WIDTH-1:0] dout;
    
    ram_init_file #(WIDTH, DEPTH) ram_init_file_dut (
        .clk(clk), 
        .rd_en(rd_en), 
        .addr(addr), 
        .dout(dout)
    );

    // Clock generation
    always #5 clk = ~clk;
    
    // Test procedure
    initial begin
        clk = 1;
        rd_en = 0;
        #14;
        rd_en = 1;
        addr = 0;
        #10;
        addr = 'd1;
        #10;
        addr = 'd2;
        #10;
        addr = 'd3;
        
        #40;
        // End simulation
        $finish;
    end
endmodule
*/

module ram_init_file_tb;
    localparam WIDTH = 8;
    localparam DEPTH = 3;
    localparam ADDR = $clog2(DEPTH);
    
    logic clk;
    logic rd_en;
    logic [ADDR-1:0] addr;
    logic [WIDTH-1:0] dout;
    
    ram_init_file #(WIDTH, DEPTH) ram_init_file_dut (
        .clk(clk), 
        .rd_en(rd_en), 
        .addr(addr), 
        .dout(dout)
    );

    // Clock generation
    always #5 clk = ~clk;
    
    // Test procedure
    initial begin
        clk = 1;
        rd_en = 0;
        #14;
        rd_en = 1;
        addr = 0;
        #10;
        addr = 'd1;
        #10;
        addr = 'd2;
        #10;
        addr = 'd3;
        
        #40;
        // End simulation
        $finish;
    end
endmodule
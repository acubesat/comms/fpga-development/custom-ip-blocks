`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12/11/2023 04:30:36 PM
// Design Name: 
// Module Name: counter_modified_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments: ETSI OPWS EINAI DOYLEYEI MIA XARA. TWRA BGAZEI 1 STO PRWTO SIGNAL OTAN EXOYN PERASEI 8 PALMOI (KAI O 9osTHA DEI SHKWMNEO TO SHMA, OMOIA GIA TO 2o SIGNAL_OUT
// 
//////////////////////////////////////////////////////////////////////////////////
module counter_modified_tb;
  // Parameters
  localparam TIMES_1 = 8;
  localparam TIMES_2 = 10;
  // Inputs
  logic clock;
  logic reset;
  logic enable;
  // Outputs
  logic out_one;
  logic out_two;
  logic [$clog2(TIMES_2):0] count; 


  // Instantiate the Counter module
  counter_modified #(TIMES_1, TIMES_2) dut (
    .clock(clock),
    .reset(reset),
    .enable(enable),
    .out_one(out_one),
    .out_two(out_two),
    .count(count)
  );

  // Clock generation
  initial begin
    clock = 1;
    forever #5 clock = ~clock;
  end

  initial begin
    enable = 0;
    reset = 0;
    #4;
    reset = 1;
    
    #20;
    enable = 1;
      
    #360
    $finish;
  end

endmodule

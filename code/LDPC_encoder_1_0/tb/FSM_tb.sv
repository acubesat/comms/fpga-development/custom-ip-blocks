`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12/11/2023 01:01:21 PM
// Design Name: 
// Module Name: FSM_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments: IT WORKS FOR NOW
// 
//////////////////////////////////////////////////////////////////////////////////
module FSM_tb;

  // Declare signals for inputs and outputs
  logic ACLOCK;
  logic ARESET, out_done, in_done, code_syst, code_output;
  logic trigger_in, trigger_out, enable;
  //GIA VERIFICATION
  logic [1:0] current;
  logic [1:0] next;

  // Instantiate FSM module
  FSM dut (
    .ACLOCK(ACLOCK),
    .ARESET(ARESET),
    .out_done(out_done),
    .in_done(in_done),
    .code_syst(code_syst),
    .code_output(code_output),
    .enable(enable),
    .trigger_in(trigger_in),
    .trigger_out(trigger_out),
      //GIA VERIFICATION
    .current(current),
    .next(next)
  );

always #5 ACLOCK = ~ACLOCK;

  initial begin
    ACLOCK = 1;
    // Reset initialization
    ARESET = 1;
    out_done = 0;
    in_done = 0;
    code_output = 0;

    // Apply reset for a few cycles
    #4;
    ARESET = 0;
    #8;
    ARESET = 1;
    // Test case 1: Transition from IDLE to IN state
    #10; // Initial wait for stable clock
    out_done = 1;
    #10;
    out_done = 0;
    #10;

    // Test case 2: Transition from IN to SYST state
    #10;
    in_done = 1;
    #10;
    in_done = 0;
    #10;

    // Test case 3: Transition from SYST to OUT state
    #10;
    code_output = 1;
    #10;
    code_output = 0;
    #10;

    // End simulation
    $finish;
  end

endmodule


//me to par state einai ayto parakatw 
/*
`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12/11/2023 01:01:21 PM
// Design Name: 
// Module Name: FSM_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments: IT WORKS FOR NOW
// 
//////////////////////////////////////////////////////////////////////////////////
module FSM_tb;

  // Declare signals for inputs and outputs
  logic ACLOCK;
  logic ARESET, out_done, in_done, par_output, code_output;
  logic trigger_in, ch_output, trigger_out;
  //GIA VERIFICATION
  logic [2:0]current;
  logic [2:0] next;

  // Instantiate FSM module
  FSM dut (
    .ACLOCK(ACLOCK),
    .ARESET(ARESET),
    .out_done(out_done),
    .in_done(in_done),
    .par_output(par_output),
    .code_output(code_output),
    .trigger_in(trigger_in),
    .ch_output(ch_output),
    .trigger_out(trigger_out),
      //GIA VERIFICATION
    .current(current),
    .next(next)
  );

always #5 ACLOCK = ~ACLOCK;

  initial begin
    ACLOCK = 1;
    // Reset initialization
    ARESET = 1;
    out_done = 0;
    in_done = 0;
    par_output = 0;
    code_output = 0;

    // Apply reset for a few cycles
    #4;
    ARESET = 0;
    #8;
    ARESET = 1;
    // Test case 1: Transition from IDLE to IN state
    #10; // Initial wait for stable clock
    out_done = 1;
    #10;
    out_done = 0;
    #10;

    // Test case 2: Transition from IN to SYST state
    #10;
    in_done = 1;
    #10;
    in_done = 0;
    #10;

    // Test case 3: Transition from SYST to PAR state
    #10;
    par_output = 1;
    #10;
    par_output = 0;
    #10;

    // Test case 4: Transition from PAR to OUT state
    #10;
    code_output = 1;
    #10;
    code_output = 0;
    #10;

    // End simulation
    $finish;
  end

endmodule


*/

`timescale 1ns / 1ps
//FORMAL ASSERTIONS FROM ARM FOR AXI4 STREAM INTERFACE

module axi_assertions_tb(
    input logic ACLOCK,
    input logic ARESETN,
    //slave interface
    input logic axis_sVALID,
    input logic axis_sREADY,
    input logic [TDATA_WIDTH-1:0]axis_sTDATA,
    //master interface
    input logic axis_mVALID,
    input logic axis_mREADY,
    input logic [TDATA_WIDTH-1:0]axis_mTDATA
    );
    
    //LOCALPARAMS
    localparam TDATA_WIDTH = 64;
    
    
  // PROP 1 Functional Rules
  // AXI4STREAM_ERRM_TVALID_RESET (if reset, no tvalid in next cycle)
  property AXI4STREAM_ERRM_TVALID_RESET;
    @(posedge ACLOCK)
      !(ARESETN) & !($isunknown(ARESETN))
      ##1   ARESETN
      |-> !axis_mVALID;
  endproperty
  axi4stream_errm_tvalid_reset: cover property (AXI4STREAM_ERRM_TVALID_RESET);
    
  // PROP 2  
  // AXI4STREAM_ERRM_TDATA_STABLE axi_master (if tready low , stable data)
  property AXI4STREAM_ERRM_TDATA_STABLE;
    @(posedge ACLOCK)
      !($isunknown({axis_mVALID,axis_mREADY,axis_mTDATA})) & ARESETN & axis_mVALID & !axis_mREADY ##1 ARESETN
      |-> $stable(axis_mTDATA);
  endproperty
  axi4stream_errm_tdata_stable: cover property (AXI4STREAM_ERRM_TDATA_STABLE);
  
  // PROP 3
  // AXI4STREAM_ERRM_TVALID_STABLE (if ready low, valid always high until tready high too)
  property AXI4STREAM_ERRM_TVALID_STABLE;
    @(posedge ACLOCK)
      ARESETN & axis_mVALID & !axis_mREADY & !($isunknown({axis_mVALID,axis_mREADY}))##1 ARESETN
      |-> axis_mVALID;
  endproperty
  axi4stream_errm_tvalid_stable: cover property (AXI4STREAM_ERRM_TVALID_STABLE);
    
  // PROP 4  X-PROP
  // INDEX: AXI4STREAM_ERRM_TDATA_X (if valid high, then no X in tdata)
  property AXI4STREAM_ERRM_TDATA_X;
    @(posedge ACLOCK)
      ARESETN & axis_mVALID
      |-> ! $isunknown(axis_mTDATA);
  endproperty
  axi4stream_errm_tdata_x: cover property (AXI4STREAM_ERRM_TDATA_X);  
  
  // Bind the assertion module to the design module
bind ldpc_2 axi_assertions_tb axi_assertions_tb(
        .ACLOCK(ACLOCK),
        .ARESETN(ARESETN),
        .axis_sVALID(axis_sVALID),
        .axis_sREADY(axis_sREADY),
        .axis_sTDATA(axis_sTDATA),
        .axis_mVALID(axis_mVALID),
        .axis_mREADY(axis_mREADY),
        .axis_mTDATA(axis_mTDATA)
); 

endmodule

 
`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12/29/2023 02:16:48 PM
// Design Name: 
// Module Name: ps_fifo_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12/29/2023 02:16:28 PM
// Design Name: 
// Module Name: sp_fifo_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module ps_fifo_tb;
    // Parameters
    localparam  WIDTHA = 4;     //64      //input (write)
    localparam DEPTHA = 3;      //80;
    localparam WIDTHB = 1;      //64   //output (read)
    localparam DEPTHB = 12;      //5120;  
    //inouts
    logic clock;
    logic reset;              //active low panta, mhn ta ksanaleme
    logic wr_en;              //otan theloyn na grapsoyn
    logic rd_en;              //otan theloyn na diabasoyn
    logic [WIDTHA-1:0] in_data;      //to be stored/writen data
    logic [WIDTHB-1:0] out_data;    //to be read data
    logic ps_fifo_empty;
    logic ps_fifo_full;

    ps_fifo #(WIDTHA, DEPTHA, WIDTHB, DEPTHB) ps_fifo_dut(
        .clock(clock),
        .reset(reset),              //active low panta, mhn ta ksanaleme
        .wr_en(wr_en),              //otan theloyn na grapsoyn
        .rd_en(rd_en),              //otan theloyn na diabasoyn
        .in_data(in_data),      //to be stored/writen data
        .out_data(out_data),    //to be read data
        .ps_fifo_empty(ps_fifo_empty),
        .ps_fifo_full(ps_fifo_full)
);

    // Clock generation
    always #5 clock = ~clock;

    // Test procedure
    initial begin
        clock = 1;
        wr_en = 0;
        rd_en = 0;
        in_data = 'd0;;
        // Reset
        reset = 0;
        #14;
        reset = 1;

        #10;        //24ns
        // Enqueue data
        wr_en = 1;
        in_data = 4'd1;     //1o
        #10;
        in_data = 4'd2;     //2o
        #10;
        in_data = 4'd3;     //3o
        #10;
        // Dequeue data
        wr_en = 0;
        rd_en = 1;
        #90;
        rd_en = 0;
        #50;

        // End simulation
        $finish;
    end

endmodule

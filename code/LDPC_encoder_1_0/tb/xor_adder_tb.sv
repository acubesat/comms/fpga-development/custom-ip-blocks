`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/15/2023 01:05:29 AM
// Design Name: 
// Module Name: xor_adder_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:        IT WORKS as expected, combinational
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module xor_adder_tb;
    logic in_bit_1;
    logic in_bit_2;
    logic out_bit;
    logic reset;

    xor_adder dut_xor_adder(
        .in_bit_1(in_bit_1),
        .in_bit_2(in_bit_2),
        .reset(reset),
        .out_bit(out_bit)
    );
 
  // Stimulus generation
  initial begin
    // Initialize inputs
    reset = 1;
    in_bit_1 = 1'b1; // case 1 0
    in_bit_2 = 1'b0; // 

    //some delay. Expected: 1111 dec 14
    #5;
    reset = 0;
    #10 reset = 1; 
    in_bit_1 = 1'b0;            //case 0 0
    #10 in_bit_1 = 1'b1;   //case 1 1
        in_bit_2 = 1'b1;
    #10 in_bit_1 = 1'b0;   //case 0 1
        in_bit_2 = 1'b1;
    
    #40
    // End simulation
    $finish;
  end   

endmodule
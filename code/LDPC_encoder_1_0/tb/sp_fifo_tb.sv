`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12/29/2023 02:16:28 PM
// Design Name: 
// Module Name: sp_fifo_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module sp_fifo_tb;
    // Parameters
    localparam  WIDTHA = 1;              //input (write)
    localparam DEPTHA = 12;      //5120;
    localparam WIDTHB = 4;     //64          //output (read)
    localparam DEPTHB = 3;     //80;
    // Signals
    logic clock;
    logic reset;              //active low panta, mhn ta ksanaleme
    logic wr_en;              //otan theloyn na grapsoyn
    logic rd_en;             //otan theloyn na diabasoyn
    logic [WIDTHA-1:0] in_data;      //to be stored/writen data
    logic [WIDTHB-1:0] out_data;    //to be read data
    logic sp_fifo_empty;
    logic sp_fifo_full;


    // Instantiate FIFO module
    sp_fifo #(WIDTHA , DEPTHA, WIDTHB, DEPTHB) sp_fifo_dut (
        .clock(clock),
        .reset(reset),
        .wr_en(wr_en),
        .rd_en(rd_en),
        .in_data(in_data),
        .out_data(out_data),
        .sp_fifo_empty(sp_fifo_empty),
        .sp_fifo_full(sp_fifo_full)
    );

    // Clock generation
    always #5 clock = ~clock;

    // Test procedure
    initial begin
        clock = 1;
        wr_en = 0;
        rd_en = 0;
        in_data = 0;
        // Reset
        reset = 0;
        #14;
        reset = 1;

        #10;        //24ns
        // Enqueue data
        wr_en = 1;
        in_data = 1'b1;     //1o
        #10;
        in_data = 1'b0;     //2o
        #10;
        in_data = 1'b0;     //3o
        #10;
        in_data = 1'b0;     //4o
                #10;
        in_data = 1'b0;     //5o
                #10;
        in_data = 1'b0;     //6o
                #10;
        in_data = 1'b1;     //7o
                #10;
        in_data = 1'b0;     //8o
        // Dequeue data
        #10;
        rd_en = 1;
        #20;
        rd_en = 0;
        #50;

        // End simulation
        $finish;
    end

endmodule

/*
//ayto htan prin xwris to bram
module sp_fifo_tb;
    // Parameters
    localparam DEPTH = 4096; //16; // Clock period in simulation (arbitrary value)

    // Signals
    logic clock;
    logic reset;
    logic wr_en;
    logic rd_en;
    logic [0:0] in_data;
    logic [3:0] out_data;
    logic sp_fifo_empty;
    logic sp_fifo_full;

    // Instantiate FIFO module
    sp_fifo #(DEPTH) sp_fifo_dut (
        .clock(clock),
        .reset(reset),
        .wr_en(wr_en),
        .rd_en(rd_en),
        .in_data(in_data),
        .out_data(out_data),
        .sp_fifo_empty(sp_fifo_empty),
        .sp_fifo_full(sp_fifo_full)
    );

    // Clock generation
    always #5 clock = ~clock;

    // Test procedure
    initial begin
        clock = 1;
        wr_en = 0;
        rd_en = 0;
        in_data = 0;
        // Reset
        reset = 0;
        #14;
        reset = 1;

        #10;        //24ns
        // Enqueue data
        wr_en = 1;
        in_data = 1'b1;     //1o
        #10;
        in_data = 1'b0;     //2o
        #10;
        in_data = 1'b0;     //3o
        #10;
        in_data = 1'b0;     //4o
                #10;
        in_data = 1'b0;     //5o
                #10;
        in_data = 1'b0;     //6o
                #10;
        in_data = 1'b1;     //7o
                #10;
        in_data = 1'b0;     //8o
        // Dequeue data
        #10;
        rd_en = 1;
        #20;
        rd_en = 0;
        #50;

        // End simulation
        $finish;
    end

endmodule

*/
`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12/18/2023 11:28:12 AM
// Design Name: 
// Module Name: accum_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module accum_tb;
    localparam TIMES = 4;
    localparam WIDTH = 4;
    localparam ACCUM_END = 4;
   logic clock;
   logic reset;
   logic enable;
   logic [WIDTH-1:0]accum_out;

    accum #(TIMES, WIDTH, ACCUM_END)accum_dut(
      .clock(clock),
      .reset(reset),
      .enable(enable),
      .accum_out(accum_out)
    );
    
  initial begin
    clock = 1;
    forever #5 clock = ~clock;
  end
  
  initial begin
    enable = 0;
    reset = 0;
    #4;
    reset = 1;
    
    #20;
    enable = 1;
      
    #360
    $finish;
  end
endmodule

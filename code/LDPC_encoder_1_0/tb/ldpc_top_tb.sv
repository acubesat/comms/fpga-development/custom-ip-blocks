`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/15/2023 12:38:49 AM
// Design Name: 
// Module Name: ldpc_top_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:     WRAIO PARADEIGMA DOYLEYEI 
// pantoy eisodos 8 bit serial apo 111111..1, perienoyme thn idia ejodo apo 10 bits 111...1 kai sto 2o intra rce perimenoyme pantoy 1 ektws apo 7o kai 8o cycle
//////////////////////////////////////////////////////////////////////////////////

module ldpc_top_tb;   
    localparam QC_COLLUMNS = 4;            //#collumns of each QC parity mini arrays
    localparam QC_ROWS = 4;               //#rows of mini QC parity arrays 
    localparam NUMBER_RCE = 2;
    localparam N = 8;
    localparam K = 10;
    localparam ACCUM_END = 4; //otan teleivsei o end, mallon tha einai to 8
    
    logic reset;
    logic enable;
    logic clock;
    logic serial_input;
    logic serial_output;
    logic counter_codeword;
    int fd = 0;

    ldpc_top#(QC_COLLUMNS, QC_ROWS, NUMBER_RCE, N, K, ACCUM_END) ldpc_top_dut(
        .ACLOCK(clock),
        .reset(reset),
        .enable(enable),
        .serial_input(serial_input),
        .serial_output(serial_output),
        .counter_codeword (counter_codeword )
    );
    
    always #5 clock = ~clock;        
    
    // Test stimulus
    initial begin
    //Open file to write and check whether its done correctly 
    fd = $fopen("Z:/vivado_proj/LDPC/LDPC_2/LDPC_2.srcs/sources_1/new/output.txt", "r+");
    if(fd)  $display("File was opened succesfully");
    else begin
        $display("File was NOT opened");
        $finish;
    end
    
    clock = 1'b1;
    reset = 1'b0;
    enable = 0;
    serial_input = 1'b0;
    
    #3 
    reset = 1;
    enable = 1;
    serial_input = 1'b1;    //1o 
    #7; 
    serial_input = 1'b1;    //2o
    #10;
    serial_input = 1'b1;    //3o
    #10;
    serial_input = 1'b1;    //3o 
        #10;
    serial_input = 1'b1;    //3o 
    #10;
    serial_input = 1'b1;    //4o
    #10;
    serial_input = 1'b1;    //5o
    #10;
    serial_input = 1'b1;    //6o
    #10;
    serial_input = 1'b1;    //7o
    #10;
    serial_input = 1'b1;    //8o
    #10;
    
    
    #110 // Wait for some time
    
    $fclose(fd);  
    $finish;
  end
    
    always@(posedge clock) begin 
        if(reset & enable & ( ldpc_top_dut.counter_modified_dut.count >=1 ) &(ldpc_top_dut.counter_modified_dut.count <=17))
            $fdisplay(fd,"%b",ldpc_top_tb.serial_output);  
    end

endmodule

`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/23/2023 02:18:47 AM
// Design Name: 
// Module Name: DFF_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
module DFF_tb;
    localparam SIZE = 1;
    reg [SIZE-1:0] D;
    logic enable;
    reg clk;
    wire [SIZE-1:0] Q;
    reg reset;
    // Instantiate the DFF module
    DFF #(SIZE)dut(
        .D(D),
        .clock(clk),
        .Q(Q),
        .enable(enable),
        .reset(reset)
    );
    
    always #5 clk = ~clk;

    // Stimulus generation
    initial begin        
        clk =0;
        enable = 1;
        reset = 1;
        // Test case 1: D transitions from 0 to 1 at rising clock edges
        D = 0;
        #10;
        repeat (5) begin
            #5;
            D = 1'd1;
            #5;
            D = 4'd0;
        end
        
        // Test case 2: Hold D at 1 for a few clock cycles
        #10;
        D = 1;
        #10;
        reset = 0;
        
        #50
        // End simulation
        $finish;
    end

endmodule







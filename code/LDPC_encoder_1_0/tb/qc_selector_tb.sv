`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12/18/2023 11:38:29 PM
// Design Name: 
// Module Name: qc_selector_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module qc_selector_tb;
    localparam INPUTS = 8;  // Number of input lines (default: 8)
    localparam TIMES = 4;    //128
    localparam WIDTH = 3;    //
    localparam ACCUM_END = 6; //otan teleivsei o end, mallon tha einai to 8

    logic [INPUTS-1:0] in;
    logic clock;
    logic reset;
    logic enable;
    logic out;   

    qc_selector#(INPUTS, TIMES, WIDTH, ACCUM_END) qc_selector_dut(
        .in(in),
        .clock(clock),
        .reset(reset),
        .enable(enable),
        .out(out)   
    );

    
    initial begin
        clock = 1;
        forever #5 clock = ~clock;
    end
    
    initial begin
    
        in = 8'b01010100;
        enable = 0;
        reset = 0;
        #4;
        reset = 1;
        
        #20;
        enable = 1;
          
        #360
        $finish;
    end

endmodule

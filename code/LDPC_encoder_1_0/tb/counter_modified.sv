`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12/11/2023 04:30:36 PM
// Design Name: 
// Module Name: counter_modified_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
module counter_modified_tb;
  // Parameters
  localparam TIMES_1 = 3;
  localparam TIMES_2 = 5;
  // Inputs
  logic clock;
  logic reset;
  // Outputs
  logic out_one;
  logic out_two;
  logic [$clog2(TIMES_2):0] count; 


  // Instantiate the Counter module
  counter_modified #(TIMES_1, TIMES_2) dut (
    .clock(clock),
    .reset(reset),
    .out_one(out_one),
    .out_two(out_two),
    .count(count)
  );

  // Clock generation
  initial begin
    clock = 0;
    forever #5 clock = ~clock;
  end

  initial begin
    reset = 0;
    #9;
    reset = 1;
      
    #90
    $finish;
  end

endmodule

`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/02/2024 05:00:36 PM
// Design Name: 
// Module Name: axis_slave_interface_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module axis_slave_fsm_tb;
    
    logic ACLOCK;
    logic ARESETN;          // Reset signal negative high
  
    logic axis_sVALID;
    logic axis_sREADY;
    
    logic ps_fifo_start;
    logic ps_fifo_full;
    logic trigger_in;
    logic in_done;

    axis_slave_fsm axis_slave_interface_dut (
        .ACLOCK(ACLOCK),
        .ARESETN(ARESETN),  
        .axis_sVALID(axis_sVALID),
        .axis_sREADY(axis_sREADY),
        .ps_fifo_start(ps_fifo_start),
        .ps_fifo_full(ps_fifo_full),
        .trigger_in(trigger_in),
        .in_done(in_done)
    );

    // Clock generation
    always #5 ACLOCK = ~ACLOCK;

    // Test procedure
    initial begin
        ACLOCK = 1;
        trigger_in = 0;
        axis_sVALID = 0;
        ps_fifo_full = 0;
        // Reset
        ARESETN = 0;
        #14;
        ARESETN = 1;

        #10;        //24ns
        axis_sVALID = 1;
        #10;
        trigger_in = 1;                
        #40;
        ps_fifo_full = 1;
        axis_sVALID = 0;
        #20;
        ps_fifo_full = 0;;
        axis_sVALID = 1;
        #10;
        trigger_in = 1;
        #10;
        ps_fifo_full = 1;
        #50

        // End simulation
        $finish;
    end
    
    
    
    
endmodule
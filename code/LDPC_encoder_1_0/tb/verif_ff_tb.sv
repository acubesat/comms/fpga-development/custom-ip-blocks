`timescale 1ns / 1ps

module verif_ff_tb;

    // Parameters
    localparam CLK_PERIOD = 10; // Clock period in ns

    // Inputs
    reg ACLOCK = 0;
    reg ARESETN = 1;
    reg axis_mVALID = 0;
    reg axis_mREADY = 0;
    reg [64-1:0] axis_mTDATA = 0;

    // Instantiate the module under test
    verif_ff dut (
        .ACLOCK(ACLOCK),
        .ARESETN(ARESETN),
        .axis_mVALID(axis_mVALID),
        .axis_mREADY(axis_mREADY),
        .axis_mTDATA(axis_mTDATA)
    );

    // Clock generation
    always #((CLK_PERIOD)/2) ACLOCK <= ~ACLOCK;

    // Stimulus
    initial begin
    ACLOCK = 1;
        // Reset
        ARESETN = 0;
        #10;
        ARESETN = 1;

        // Wait a few clock cycles
        #50;

        // Provide valid data
        axis_mVALID = 1;
        axis_mREADY = 1;
        axis_mTDATA = 64'hABCDEABCDEABCDE; // Example data
        
        #10;
        axis_mVALID = 0;
        axis_mTDATA = 64'hFFFFFFFFFFFFFFF; // Example data
        #10;
        axis_mVALID = 1;
        #10;
        axis_mVALID = 0;
        axis_mTDATA = 64'h0; // Example data
        #10;
        // Wait for a few clock cycles
        #100;

        // End simulation
        $finish;
    end

endmodule

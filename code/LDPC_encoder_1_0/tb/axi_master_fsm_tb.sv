`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/02/2024 05:00:36 PM
// Design Name: 
// Module Name: axis_slave_interface_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
module axis_master_fsm_tb;
    logic ACLOCK;
    logic ARESETN;          // Reset signal negative high
    logic axis_sVALID;
    logic axis_sREADY;    
    logic sp_fifo_start;
    logic sp_fifo_empty;
    logic sp_fifo_full;
    logic trigger_out;
    logic out_done;
 
    axis_master_fsm axis_master_dut (
        .ACLOCK(ACLOCK),
        .ARESETN(ARESETN),  
        .axis_sVALID(axis_sVALID),
        .axis_sREADY(axis_sREADY),
        .sp_fifo_start(sp_fifo_start),
        .sp_fifo_empty(sp_fifo_empty),
        .sp_fifo_full(sp_fifo_full),
        .trigger_out(trigger_out),
        .out_done(out_done)
    );

    // Clock generation
    always #5 ACLOCK = ~ACLOCK;

    // Test procedure
    initial begin
        ACLOCK = 1;
        trigger_out = 0;
        axis_sREADY = 0;
        sp_fifo_full = 0;
        // Reset
        ARESETN = 0;
        #14;
        ARESETN = 1;

        #10;        //24ns
        axis_sREADY = 1;
        #10;
        trigger_out = 1;                
        #40;
        sp_fifo_full = 1;
        axis_sREADY = 0;
        #20;
        sp_fifo_full = 0;;
        axis_sREADY = 1;
        #10;
        trigger_out = 1;
        #10;
        sp_fifo_full = 1;
        #50

        // End simulation
        $finish;
    end
    
    
    
    
endmodule
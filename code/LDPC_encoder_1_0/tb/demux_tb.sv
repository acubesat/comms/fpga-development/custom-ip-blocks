`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/27/2023 02:17:19 AM
// Design Name: 
// Module Name: demux_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module demux_tb;
    localparam SIZE_A = 1;
    localparam SIZE_B = 1;
    
    logic [SIZE_A-1:0]a;
    logic [SIZE_B-1:0]b;
     logic choise;
     logic reset;
     logic [SIZE_A-1:0]out;
    
    demux#(SIZE_A, SIZE_B) demux_dut(
        .a(a),
        .b(b),
        .choise(choise),
        .reset(reset),
        .out(out)
    );
    
    initial begin
    reset = 1'b0;
    a = 1;
    b = 1'b0;
    
    #3 
    choise = 1;
    reset = 1'b1;
    #7; 
    choise = 1'b0;
    #10;
    choise = 1'b1;
    
    #60 // Wait for some time
    $finish;
  end    
       
endmodule

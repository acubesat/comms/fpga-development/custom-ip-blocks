`timescale 1ns / 1ps

module OQPSK_MODULATOR_v2_0 #(

        // Slave port 0 parameters.
        parameter C_S00_AXIS_TDATA_WIDTH = 8,
        
        // Master port 0 parameters. Associated with the FIR handling the in-phase component.
        parameter C_M00_AXIS_TDATA_WIDTH = 16,
        
        // Master port 1 parameters. Associated with the FIR handling the quadrature component.
        parameter C_M01_AXIS_TDATA_WIDTH = 16
        
    )(

        /* General inputs and outputs. */
        input logic aclk,
        input logic aresetn,

        /* AXI4-S slave port 0 inputs and outputs. */
        input logic s00_axis_tvalid,
        input logic [C_S00_AXIS_TDATA_WIDTH - 1 : 0] s00_axis_tdata,
        output logic s00_axis_tready,
        input logic [C_S00_AXIS_TDATA_WIDTH / 8 - 1 : 0] s00_axis_tkeep,
        input logic s00_axis_tlast,
        
        /* AXI4-S master port 0 inputs and outputs. */
        output logic  m00_axis_tvalid,
        output logic [C_M00_AXIS_TDATA_WIDTH - 1 : 0] m00_axis_tdata,
        input logic m00_axis_tready,
        output logic m00_axis_tlast,

        /* AXI4-S master port 1 inputs and outputs. */
        output logic  m01_axis_tvalid,
        output logic [C_M01_AXIS_TDATA_WIDTH - 1 : 0] m01_axis_tdata,
        input logic m01_axis_tready,
        output logic m01_axis_tlast
    
    );
    
    localparam C_S00_AXIS_TKEEP_WIDTH = C_S00_AXIS_TDATA_WIDTH / 8;
    localparam SAMPLES_PER_SYMBOL = 4;
    localparam COMPONENT_SIZE = C_S00_AXIS_TDATA_WIDTH*SAMPLES_PER_SYMBOL + SAMPLES_PER_SYMBOL / 2;
    localparam RECEIVE_DATA_POINT = COMPONENT_SIZE >> 1;
    localparam COUNTER_SIZE = $clog2(RECEIVE_DATA_POINT);
    localparam SYMBOL_SIZE = 2;
    

    localparam SqrtTwoOverTwoPos = 16'b0101101000010010; // Is +0.70709228515625 approximately +sqrt(2)/2.
    localparam SqrtTwoOverTwoNeg = 16'b1010010111101110; // Is -0.70709228515625 approximately -sqrt(2)/2.

    localparam RESET_STATE = C_S00_AXIS_TDATA_WIDTH + 2;
    

    
    /* 
     * Logic for the validity of the data sent from the masters.
     * This block is triggered when valid data is received and for the 
     * duration the data is mapped and sent to the FIR compilers.
     */
    logic MasterWriteReadiness;
    always_comb begin
        if (InPhaseIndex != RESET_STATE) begin
            m00_axis_tvalid = 1;
            m01_axis_tvalid = 1; 
        end
        else begin
            m00_axis_tvalid = 0;
            m01_axis_tvalid = 0;
        end
        MasterWriteReadiness = m01_axis_tready && m00_axis_tready && m00_axis_tvalid && m01_axis_tvalid;
    end

    /* 
     * This is a the condition for which the module is (re)set to a known state, from which
     * the module can expect data. The *aresetn* input is an active low reset and must therefore 
     * be negated.
     * Signal SlaveReceiveReadiness is set to high when a handshake is successful between
     * the sending master and receiving slave. Otherwise the previous state is preserved.
     */
    logic ResetCondition;
    logic SlaveReceiveReadiness;
        assign SlaveReceiveReadiness = s00_axis_tready & s00_axis_tvalid;
    always_ff @(posedge aclk) begin
        ResetCondition <= !aresetn;
    end

    /* 
     * Logic for receiving data. The data seen in the s00_axis_tdata input is stored in 
     * register ValidInput only if the SlaveReceiveReadiness signal is set high.
     */
    logic [C_S00_AXIS_TDATA_WIDTH - 1 : 0] ValidInput;
    always_ff @(posedge aclk) begin
        if (SlaveReceiveReadiness) begin
            ValidInput <= s00_axis_tdata;
        end
        else begin
            ValidInput <= ValidInput;       
        end
    end

    /* 
     * The two follwing blocks create a multiplexer to determine the behaviour of the sample
     * counters. A handshake is performed to determine if the slave is ready to receive data.
     * If not the counter maintains its current value until the slave is ready to receive. If 
     * the slave is ready, the special case where the last data transfer has already taken place,
     * is examined. Setting the *InPhaseSampleCounter* to *SAMPLES_PER_SYMBOL - 1* serves to 
     * set the last two clock cycles to zero, since the OQPSK demands so. Respectively, for the
     * *QuadratureSampleCounter* the case of first transfer is examined.
     */
    logic [1:0] InPhaseSampleCounter;
    logic [1:0] QuadratureSampleCounter;
    logic [COUNTER_SIZE - 1 : 0] InPhaseIndex;
    logic [COUNTER_SIZE - 1 : 0] QuadratureIndex;
    always_ff @(posedge aclk) begin  
        if (ResetCondition) begin
            InPhaseSampleCounter <= 0;
        end
        else begin
            if (MasterWriteReadiness) begin
                InPhaseSampleCounter <= InPhaseIndex == C_S00_AXIS_TDATA_WIDTH ? SAMPLES_PER_SYMBOL - 1 : InPhaseSampleCounter + 1'b1;
            end
            else begin
                InPhaseSampleCounter <= InPhaseSampleCounter;
            end
        end
    end

    always_ff @(posedge aclk) begin  
        if (ResetCondition) begin
            QuadratureSampleCounter <= 0;
        end
        else begin
            if (MasterWriteReadiness) begin
                if (QuadratureSampleCounter == SAMPLES_PER_SYMBOL - 1) begin
                    QuadratureSampleCounter <= 0;    
                end
                else begin
                    QuadratureSampleCounter <= QuadratureIndex == 0 ? SAMPLES_PER_SYMBOL - 1 : QuadratureSampleCounter + 1'b1;
                end
            end
            else begin
                QuadratureSampleCounter <= QuadratureSampleCounter;
            end
        end
    end


    /*
     * The following two blocks determine the behaviour of the indexes for each component.
     * In case the slave is not ready to receive, the previous state is preserved. Otherwise, 
     * if the sample counter is *SAMPLES_PER_SYMBOL - 1*, meaning that all *SAMPLES_PER_SYMBOL*
     * samples for the current symbol have been sent, the index is increased by two in order to
     * fetch the next symbol. 
     */
    always_ff @(posedge aclk) begin
        if (ResetCondition) begin
            InPhaseIndex <= RESET_STATE;
        end
        else begin
            if (~SlaveReceiveReadiness & MasterWriteReadiness) begin
                InPhaseIndex <= InPhaseSampleCounter == SAMPLES_PER_SYMBOL - 1 ? InPhaseIndex + 2'b10 : InPhaseIndex;
            end
            else if (SlaveReceiveReadiness & ~MasterWriteReadiness) begin
                InPhaseIndex <= 0;
            end
            else begin
                InPhaseIndex <= InPhaseIndex;
            end
        end
    end

    always_ff @(posedge aclk) begin
        if (ResetCondition) begin
            QuadratureIndex <= RESET_STATE;
        end
        else begin
            if (~SlaveReceiveReadiness & MasterWriteReadiness) begin
                QuadratureIndex <= QuadratureSampleCounter == SAMPLES_PER_SYMBOL - 1 ? QuadratureIndex + 2'b10 : QuadratureIndex;
            end
            else if (SlaveReceiveReadiness & ~MasterWriteReadiness) begin
                QuadratureIndex <= 0;
            end
            else begin
                QuadratureIndex <= QuadratureIndex;
            end
        end
    end

    /*
     * The slave is ready to receive in case both *InPhaseSampleCounter* and *InPhaseIndex* are
     * reset to the known state of zero and not set to any other value since otherwise these is 
     * a transfer underway.  
     */
    always_comb begin
        if (InPhaseIndex == RESET_STATE) begin
            s00_axis_tready = 1;
        end
        else begin
            s00_axis_tready = 0;
        end 
    end

    /*
     * Determines what data is going to be sent by the master based on where in the FSM it is, 
     * as per the demands of the OQPSK modulator. For the In-phase, the last two clock cycles
     * are zero, whereas for the Quadrature the first two clocks are zero.   
     */
    always_comb begin
        if (InPhaseIndex == C_S00_AXIS_TDATA_WIDTH) begin 
            m00_axis_tdata = 0;
        end
        else begin
            m00_axis_tdata = ValidInput[InPhaseIndex] ? SqrtTwoOverTwoPos : SqrtTwoOverTwoNeg;
        end
    end

    always_comb begin
        if (QuadratureIndex == 0) begin
            m01_axis_tdata = 0;
        end
        else begin
            m01_axis_tdata = ValidInput[QuadratureIndex + 1 - SAMPLES_PER_SYMBOL / 2] ? SqrtTwoOverTwoPos : SqrtTwoOverTwoNeg;
        end
    end

    /* 
     * Last transfer is signaled when the last sample of the last symbol has been sent.
     */
    always_comb begin
        m00_axis_tlast = InPhaseIndex == C_S00_AXIS_TDATA_WIDTH && QuadratureSampleCounter == SAMPLES_PER_SYMBOL - 1 ? 1 : 0;
        m01_axis_tlast = InPhaseIndex == C_S00_AXIS_TDATA_WIDTH && QuadratureSampleCounter == SAMPLES_PER_SYMBOL - 1 ? 1 : 0;         
    end
    
endmodule
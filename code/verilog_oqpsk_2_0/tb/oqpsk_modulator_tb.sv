`timescale 1ns / 1ps
`include "src/oqpsk_modulator.sv" 
module OQPSK_MODULATOR_v2_0_tb;

/* Parameters of the OQPSK_MODULATOR_v2_0 instance. */
localparam C_S00_AXIS_TDATA_WIDTH = 8;
localparam C_M00_AXIS_TDATA_WIDTH = 16;
localparam C_M01_AXIS_TDATA_WIDTH = 16;

/* General purpose signals. */
logic aclk;
logic aresetn;

/* Slave ports */
logic s00_axis_tvalid;
logic [C_S00_AXIS_TDATA_WIDTH-1:0] s00_axis_tdata;
logic [C_S00_AXIS_TDATA_WIDTH/8-1:0] s00_axis_tkeep;
logic s00_axis_tlast;
logic s00_axis_tready;

/* Master ports */
logic m00_axis_tvalid;
logic [C_M00_AXIS_TDATA_WIDTH-1:0] m00_axis_tdata;
logic m00_axis_tready;
logic m00_axis_tlast;

/* Master ports */
logic m01_axis_tvalid;
logic [C_M01_AXIS_TDATA_WIDTH-1:0] m01_axis_tdata;
logic m01_axis_tready;
logic m01_axis_tlast;

/* Instantiation of the OQPSK_MODULATOR_v2_0 module */
OQPSK_MODULATOR_v2_0 #(
    .C_S00_AXIS_TDATA_WIDTH(C_S00_AXIS_TDATA_WIDTH),
    .C_M00_AXIS_TDATA_WIDTH(C_M00_AXIS_TDATA_WIDTH),
    .C_M01_AXIS_TDATA_WIDTH(C_M01_AXIS_TDATA_WIDTH)
) modulator_inst (
    .aclk(aclk),
    .aresetn(aresetn),
    .s00_axis_tvalid(s00_axis_tvalid),
    .s00_axis_tdata(s00_axis_tdata),
    .s00_axis_tready(s00_axis_tready),
    .s00_axis_tkeep(s00_axis_tkeep),
    .s00_axis_tlast(s00_axis_tlast),
    .m00_axis_tvalid(m00_axis_tvalid),
    .m00_axis_tdata(m00_axis_tdata),
    .m00_axis_tready(m00_axis_tready),
    .m00_axis_tlast(m00_axis_tlast),
    .m01_axis_tvalid(m01_axis_tvalid),
    .m01_axis_tdata(m01_axis_tdata),
    .m01_axis_tready(m01_axis_tready),
    .m01_axis_tlast(m01_axis_tlast)
);

/* Clock generation */
initial begin
    aclk = 0;
    forever #5 aclk = ~aclk;
end


initial begin
    /* Simulation */
    $dumpfile("oqpsk_results.vcd");
    $dumpvars(0, OQPSK_MODULATOR_v2_0_tb);
    
    /* Initializing signals */
    m00_axis_tready = 1;
    m01_axis_tready = 1;
    s00_axis_tkeep = -1;
    s00_axis_tlast = 0;
    s00_axis_tvalid = 0;

    /* Reset the peripheral */
    aresetn = 0;
    #10;
    aresetn = 1;
    #105;

    /* Wait for the next positive edge of the clock */
    /* Send valid data from master */
    s00_axis_tdata = 64'b1010_0101_1010_0101_1010_0101_1010_0101_1010_0101_1010_0101_1010_0101_1010_0101;    
    s00_axis_tlast = 1;
    s00_axis_tvalid = 1;
    #10;
    s00_axis_tvalid = 0;
    s00_axis_tlast = 0;
    /* Wait for completion */
    #1400;

    /* Send valid data from master */
    s00_axis_tvalid = 1;
    s00_axis_tdata = 64'b1010_0101_1010_0101_1010_0101_1010_0101_1010_0101_1010_0101_1010_0101_1010_1010;
    s00_axis_tlast = 1;
    #10;
    s00_axis_tvalid = 0;
    s00_axis_tlast = 0;

    /* Wait for completion */
    #1400;
    $finish;
end

endmodule
